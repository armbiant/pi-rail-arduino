/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <PString.h>

#include <EEPROM.h>
#include <FS.h>

#include <SPIFFS.h>

#include <WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <Servo.h>
#include <driver/adc.h>

#include <QName.h>
#include <XmlPiRail.h>
#include <PiHttpServer.h>
#include <PiOTA.h>

int INFO_LED = 2; //INFO_LED;

int LED_ON = HIGH;
int LED_OFF = LOW;

//----- Constants and variable for WiFi
const char *wifiModuleType = __FILE__;
int wifiStatus = WL_IDLE_STATUS;

Data* data = NULL;

#define HOSTNAME_SIZE 32
#define ERROR_MSG_SIZE 64
const int UDP_BUF_SIZE = 1500;

char inputString[UDP_BUF_SIZE + 1]; // a string to hold incoming data
char myHostname[HOSTNAME_SIZE + 1];
boolean stringComplete = false;        // whether the string is complete
int inputPos;

int nextPwmChannel = 0; // used for motor and PWM pins

WiFiUDP Udp;
char udpOut[UDP_BUF_SIZE+1];

HardwareSerial HwSerial1(1);
HardwareSerial HwSerial2(2);
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int nextServo = 1;

const PROGMEM char *sketchFileNameAndCompileDate = __FILE__ " " __DATE__ " " __TIME__;
const char *sketchNameAndCompileDate;

bool initialConfigMode = false;
char lastErrorMsg[ERROR_MSG_SIZE + 1];

static PROGMEM const char *PI_SETUP = "Pi-Setup";

XmlPiRail xmlPiRail( wifiModuleType, 1024, UDP_BUF_SIZE );
XmlReader xmlSyncReader( UDP_BUF_SIZE );
XmlWriter xmlCmdWriter( UDP_BUF_SIZE );
char xmlSyncReadBuffer[2*(UDP_BUF_SIZE+1)];

State udpState;
const QName* locoID = NULL;
Cmd* locoMotorCmd = NULL;
Cmd* locoLightCmd = NULL;
int locoCmdCount = 5;
uint32_t locoIPAddr = 0;
int targetSpeed = -1;
char targetDir = 0;

PiHttpServer piHttpServer;
bool httpServerWasAccessed = false;

PiOTA piOta;

bool inputEventOccurred = false;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
//variable für Hebel
int Fahrhebel;
int gstufe;
unsigned long lastSend = 0;
/*
//-------------------------------------------------
void IRAM_ATTR eventIsr(void *arg) {
  // Die ISR-Routine muss ohne fremden Code auskommen!
  // Der Grund:
  // Sowohl XmlPiRail als auch die Model-klassen haben NICHT das IRAM_ATTR, dadurch müssen
  // sie ggf. erst aus dem Flash-Speicher nachgeaden werden und dann dauert das zu lange
  // Symptom: Ich hatte gleich am Anfang von xmlPiRail.addEvent() ein Serial.println() drin
  // und das kam manchmal und manchmal nicht.
  portENTER_CRITICAL( &mux );
  InCfg *inCfg = static_cast<InCfg*>(arg);
  inputEventOccurred = true;
  inCfg->eventOccurred = true;
  portEXIT_CRITICAL( &mux );
}
*/
//-------------------------------------------------
void fatalError( const __FlashStringHelper* msg ) {
  Serial.println( msg );
  Serial.println( F("Stopped - try to fix config") );
  xmlPiRail.setErrorMode( true );
  xmlPiRail.setNextSyncTime( millis() );
  int count = strlen_P( (PGM_P) msg );
  if (count > ERROR_MSG_SIZE) {
    count = ERROR_MSG_SIZE;
  }
  memcpy_P( lastErrorMsg, msg, count );
  lastErrorMsg[count] = 0;
}

unsigned long resetMillisStart = 0L;
unsigned long resetAfterMillis = 0L;

//-------------------------------------------------
void printHeap( const __FlashStringHelper* msg ) {
  Serial.print( F("ms=") );
  Serial.print( millis() );
  Serial.print( " " );
  Serial.print( msg );
  Serial.print( F(", heap=") );
  Serial.println( ESP.getFreeHeap() );
}

//-------------------------------------------------
const char* getVersion() {
  return __DATE__;
}

//-------------------------------------------------
int getHeapSize() {
  int heap = ESP.getFreeHeap();
  return heap;
}

//-------------------------------------------------
void doReset( long afterMillis ) {
  resetMillisStart = millis();
  resetAfterMillis = afterMillis > 0 ? afterMillis : 250; // have some minimum value
  Serial.println();
  Serial.print( F("Requested reset ") );
  Serial.print( afterMillis );
  Serial.print( F(" ms after ") );
  Serial.println( resetMillisStart );
}

//-------------------------------------------------
void httpServerAccessed() {
  httpServerWasAccessed = true;
}

int ledSpeedDefault = 250;

int ledSpeed = ledSpeedDefault;
int ledState = LED_ON;
unsigned long lastToggle = 0;

//-------------------------------------------------
bool toggleLed() {
  unsigned long now = millis();

  if (!lastToggle || now - lastToggle > ledSpeed) {
    ledState = ledState == LED_ON ? LED_OFF : LED_ON;
    digitalWrite( INFO_LED, ledState );
    lastToggle = now;
    return true;
  }
  else {
    return false;
  }
}

//-------------------------------------------------
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print( F("SSID: ") );
  Serial.println( WiFi.SSID() );

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print( F("IP: ") );
  Serial.print( ip.toString() );

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print( F(" RSSI: ") );
  Serial.print( rssi );
  Serial.println( F(" dBm") );
}

//-------------------------------------------------
boolean connectToWifi() {
  const char* ssid = xmlPiRail.getSsid();
  Serial.print( "ssid=" );  Serial.println( ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    return false;
  }
  const char* pass = xmlPiRail.getPass();

  //--- connect to Wifi
  Serial.print( F("Connecting to ") );
  Serial.print( ssid );
  WiFi.setHostname( myHostname );

  // Delay based on MAC to avoid all devices connecting same time
  Serial.print( F(", delay=") );
  uint8_t mac[6];
  WiFi.macAddress( mac );
  int waitConn = mac[5] * 4;
  Serial.println( waitConn );
  delay( waitConn );

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin( ssid, pass );

  ledSpeed = 200;
  unsigned long startMillis = millis();

  while (WiFi.status() != WL_CONNECTED && millis() - startMillis < 60 * 1000) {
    Serial.print(".");
    delay(200);
    toggleLed();
  }

  bool success = WiFi.status() == WL_CONNECTED;

  if (success) {
    Serial.println();
    Serial.println( F("Connected") );
    printWifiStatus();
  }
  else {
    Serial.println();
    Serial.println( F("WiFi failure") );
    WiFi.disconnect( true, false );
  }
  digitalWrite(INFO_LED, success ? LED_OFF : LED_ON);

  return success;
}

boolean isValidNameChar( char ch ) {
  return ( ((ch == '-') || (ch == '_'))
           || ((ch >= '0') && (ch <= '9'))
           || ((ch >= 'A') && (ch <= 'Z'))
           || ((ch >= 'a') && (ch <= 'z'))
         );
}

void setUpHostname( const char * deviceId, boolean macPrefix ) {
  memset( myHostname, 0, HOSTNAME_SIZE + 1 );
  int pos = 0;
  int i = 0;
  if (macPrefix) {
    String mac = WiFi.macAddress();
    while (pos < mac.length()) {
      char ch = mac.charAt(pos);
      if (isValidNameChar( mac[pos] )) {
        myHostname[i++] = ch;
      }
      pos++;
    }
    myHostname[i++] = '-';
  }
  if (deviceId != NULL) {
    pos = 0;
    while ((i < HOSTNAME_SIZE - 1) && (deviceId[pos] > 0)) {
      char ch = deviceId[pos];
      if (isValidNameChar( ch )) {
        myHostname[i++] = ch;
      }
      else {
        myHostname[i++] = '-';
      }
      pos++;
    }
  }
}

void setUpConfigWifi() {
  digitalWrite( INFO_LED, HIGH );
  Serial.println();
  Serial.print( F("Setup AP: ") );
  Serial.println( myHostname );

  WiFi.softAP( myHostname );

  IPAddress wifiLocalIp = WiFi.softAPIP();
  Serial.print( F("AP IP=") );
  Serial.println( wifiLocalIp );
}

//--------------------------
void initOutput( OutCfg* outCfg ) {
  int pin = outCfg->getPin();
  outType_t pinType = outCfg->getType();
  if (pin < 0x100) {
    switch (pinType) {
      case OUTTYPE_LOGIC:
      case OUTTYPE_HIGHSIDE:
      case OUTTYPE_LOWSIDE:
      case OUTTYPE_HALFBRIDGE: {
        pinMode( pin, OUTPUT );
        digitalWrite( pin, LOW );
        break;
      }
      case OUTTYPE_SERVO: {
        if (nextServo == 1) {
          servo1.attach( pin, nextPwmChannel );
        }
        else if (nextServo == 2) {
          servo2.attach( pin, nextPwmChannel );
        }
        else if (nextServo == 3) {
          servo3.attach( pin, nextPwmChannel );
        }
        else if (nextServo == 4) {
          servo4.attach( pin, nextPwmChannel );
        }
        else {
          Serial.println( F("ERROR too much servos!") );
          return;
        }
        outCfg->index = nextServo;
        outCfg->pwmChannel = nextPwmChannel;
        Serial.print( F("  Servo") );
        Serial.print( nextServo );
        Serial.print( F(", ch=") );
        Serial.println( nextPwmChannel );
        nextPwmChannel++;
        nextServo++;
        break;
      }
      default: {
        Serial.println( "unsupported pinType" );
      }
    }
  }
  else {
    Serial.println( F("--TODO-- init I2C port") );
  }
}

void initInput( InCfg* pinCfg ) {
  int pin = pinCfg->getPin();
  if (pinCfg->getPullup()) {
    pinMode( pin, INPUT_PULLUP );
  }
  else {
    pinMode( pin, INPUT );
  }
}

bool attachOutPin( OutCfg* outCfg, outMode_t outMode ) {
  ExtCfg *extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    if (outCfg->attached == OUTMODE_NONE) {
      if (outMode == OUTMODE_PWM) {
        if (outCfg->pwmChannel == -1) {
          int pin = outCfg->getPin();
          pinMode( pin, OUTPUT );
          outCfg->pwmChannel = nextPwmChannel;
          ledcSetup( nextPwmChannel, 5000, 10 );
          ledcAttachPin( pin, nextPwmChannel );
          Serial.print(F( "  PWM, ch=" ));
          Serial.println( nextPwmChannel );
          nextPwmChannel++;
        }
      }
      outCfg->attached = outMode;
      return true;
    }
    else {
      return (outMode == outCfg->attached);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (outCfg->attached == OUTMODE_NONE) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return (outCfg->attached == outMode);
      }
    }
  }
}

bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useIterrupt ) {
  if (inCfg != NULL) {
    if (inCfg->attached == INMODETYPE_NONE) {
      if (inMode == INMODETYPE_DIGITAL) {
        int pin = inCfg->getPin();
        if (pin > 0) {
          /*
          if (useIterrupt) {
            switch (fireType) {
              case FIRETYPE_ONFALL: {
                attachInterruptArg( pin, eventIsr, inCfg, FALLING );
                break;
              }
              case FIRETYPE_ONRISE: {
                attachInterruptArg( pin, eventIsr, inCfg, RISING );
                break;
              }
              default: {
                attachInterruptArg( pin, eventIsr, inCfg, CHANGE );
              }
            }
            Serial.print( "Attached interrupt to pin=" );
            Serial.println( pin );
          }
          else{*/
            Serial.print( "Activated input polling for pin=" );
            Serial.println( pin );
          //}
        }
        else {
          Serial.print( "Pin missing in inCfg " );
          xmlPiRail.serialPrintlnQName( inCfg->getId() );
        }
      }
      inCfg->attached = inMode;
      return true;
    }
    else {
      return (inCfg->attached == inMode);
    }
  }
}

bool attachPort( PortCfg* portCfg, portMode_t portMode ) {
  ExtCfg *extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_NONE) {
      if (portCfg->getType() == PORTTYPE_SERIALMODULATED) {
        int pwmPin = portCfg->getPinByType( PORTPINTYPE_CLOCKPIN );
        Serial.print("  pwm=");
        Serial.print( pwmPin );
        int rxPin = portCfg->getPinByType( PORTPINTYPE_RX_PIN );
        Serial.print(", rx=");
        Serial.print(rxPin );
        int txPin = portCfg->getPinByType( PORTPINTYPE_TX_PIN );
        Serial.print(", tx=");
        Serial.print( txPin );
        int baudRate = portCfg->getParamIntVal( piRailFactory->ID_BAUD_RATE );
        Serial.print(", baud=");
        Serial.print( baudRate );
        int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
        if (index == 1) {
          Serial.print( F(", port=1...") );
          HwSerial1.begin( baudRate, SERIAL_8N1, rxPin, txPin );
          Serial.println( F("ok") );
        }
        else if (index == 2) {
          Serial.print( F(", port=2...") );
          HwSerial2.begin( baudRate, SERIAL_8N1, rxPin, txPin );
          Serial.println( F("ok") );
        }
        else {
          Serial.print( F("Invalid msg index (1..2): ") );
          Serial.println( index );
          return false;
        }
        int pwmChannel = nextPwmChannel++;
        ledcSetup( pwmChannel, 38000, 8 ); // 38 kHz PWM, 8-bit resolution
        ledcAttachPin( pwmPin, pwmChannel ); // assign a led pins to a channel
        ledcWrite( pwmChannel, 100 );
        portCfg->attached = portMode;
        return true;
      }
      else {
        Serial.println( F("Unsupported port type") );
        return false;
      }
    }
    else {
      return (portCfg->attached == portMode);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (portCfg->attached == PORTMODE_NONE) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return (portCfg->attached == portMode);
      }
    }
  }
}

//--------------------------
void setup() {
  sketchNameAndCompileDate = strrchr( sketchFileNameAndCompileDate, '/' );
  if (sketchNameAndCompileDate) {
    sketchNameAndCompileDate ++;
  }
  else {
    sketchNameAndCompileDate = sketchFileNameAndCompileDate;
  }

  pinMode(INFO_LED, OUTPUT);     // Initialize the INFO_LED pin as an output
  digitalWrite(INFO_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  lastErrorMsg[0] = 0;

  pinMode(18, OUTPUT);
  digitalWrite(18, HIGH);  // Activate H-Bridges

  Serial.begin( 115200 );
  inputPos = 0;
  Serial.println();
  Serial.println( sketchNameAndCompileDate );
  Serial.println();
  printHeap( F("Begin setup") );

  piRailFactory->max_csv_count = 5;
  piRailFactory->max_ev_count = 4;
  piRailFactory->max_fileDef_count = 10;

  //--- From https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html :
  // Please do not use the interrupt of GPIO36 and GPIO39 when using ADC or Wi-Fi with sleep mode enabled.
  // Please refer to the comments of adc1_get_raw. Please refer to section 3.11 of 
  // ‘ECO_and_Workarounds_for_Bugs_in_ESP32’ for the description of this issue. 
  // As a workaround, call adc_power_acquire() in the app. This will result in higher power consumption
  // (by ~1mA), but will remove the glitches on GPIO36 and GPIO39.
  adc_power_acquire();

  data = new Data();
  xmlPiRail.setup( WiFi.macAddress().c_str(), DEVICETYPE_CONTROLLER );

  // Ensure we have a hostname before connectToWifi()
  Cfg* cfg = xmlPiRail.getCfg();
  const char *deviceName;
  if ((cfg != NULL) && (cfg->getId() != NULL) && (strlen(cfg->getId()->getName()) > 0)) {
    deviceName = cfg->getId()->getName();
    setUpHostname( deviceName, false );
  }
  else {
    deviceName = xmlPiRail.getDeviceID();
    if (deviceName == NULL) {
      deviceName = "INIT";
    }
    setUpHostname( deviceName, true );
  }

  if (!initialConfigMode) {
    initialConfigMode = !connectToWifi();
  }

  if (initialConfigMode) {
    // discard the hostname and rebuild
    setUpHostname( deviceName, true );
  }

  Serial.print( F("Hostname=") );
  Serial.println( myHostname );

  if (initialConfigMode) {
    setUpConfigWifi();
  }

  delay(250);

  FileCfg* fileCfg = xmlPiRail.getFileCfg();
  NetCfg* netCfg = xmlPiRail.getNetCfg();
  piHttpServer.init( sketchNameAndCompileDate, myHostname, fileCfg, netCfg );
  piHttpServer.begin();

  piOta.begin( myHostname );

  memset( xmlSyncReadBuffer, 0, UDP_BUF_SIZE + 1 );
  
  // TODO Konfigurierbar machen: Datenstrukturen für Motor- und Licht-Kommandos
  locoID = piRailFactory->NS_PIRAILFACTORY->getQName( "98-CD-AC-CF-F8-38" ); //BR-147-557
  //locoID = piRailFactory->NS_PIRAILFACTORY->getQName( "08-3A-F2-14-EC-5C" ); //LokG2
  const QName* motorID = piRailFactory->NS_PIRAILFACTORY->getQName( "Motor");
  locoMotorCmd = piRailFactory->getNewCmd( NULL, piRailFactory->ID_CMD );
  locoMotorCmd->setSender( xmlPiRail.getDeviceState()->getId() );
  LockCmd* lckCmd = (LockCmd*) locoMotorCmd->createChild( piRailFactory->ID_LCK );
  lckCmd->setId( motorID );
  lckCmd->setApply( true );
  locoMotorCmd->addChild( piRailFactory->ID_LCK, lckCmd, NULL );
  MoCmd* moCmd = (MoCmd*) locoMotorCmd->createChild( piRailFactory->ID_MO );
  moCmd->setId( motorID );
  locoMotorCmd->addChild( piRailFactory->ID_MO, moCmd, lckCmd );

  const QName* lightID = piRailFactory->NS_PIRAILFACTORY->getQName( "Light");
  locoLightCmd = piRailFactory->getNewCmd( NULL, piRailFactory->ID_CMD );
  locoLightCmd->setSender( xmlPiRail.getDeviceState()->getId() );
  lckCmd = (LockCmd*) locoLightCmd->createChild( piRailFactory->ID_LCK );
  lckCmd->setId( lightID );
  lckCmd->setApply( true );
  locoLightCmd->addChild( piRailFactory->ID_LCK, lckCmd, NULL );
  SetCmd* setCmd = (SetCmd*) locoLightCmd->createChild( piRailFactory->ID_SET );
  setCmd->setId( lightID );
  setCmd->setValue( '0' );
  setCmd->setIntVal( 0 );
  locoLightCmd->addChild( piRailFactory->ID_SET, setCmd, lckCmd );

  if (!initialConfigMode && !xmlPiRail.isErrorMode()) {
    Serial.print( F("Start listen UDP ") );
    Serial.println( piRailSendPort );
    // if you get a connection, report back via serial:
    Udp.begin( piRailSendPort );
    xmlPiRail.begin( udpOut, -1 );
  }

  printHeap( F("Finished setup") );
}

//------------------------------------------------------------------
// Hardware access functions
//------------------------------------------------------------------

int readMotorSensor( PortCfg* portCfg, int currentSpeed ) {
  // not supported
  return 0;
}

void processMsgState( MsgState* newMsgState ) {
  // not supported
}

const QName* checkIDReader( SensorAction* sensorAction ) {
  // not supported
  return NULL;
}

void doMotor( PortCfg* portCfg, char dir, int motorOutput ) {
  // not supported
}

void doOutputPin( OutCfg* outCfg, int pinValue, int duration ) {
  if (outCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    int pin = outCfg->getPin();
    if (pin < 0) {
      return;
    }
    outMode_t outMode = outCfg->attached;
    switch (outMode) {
      case OUTMODE_SERVO: {
        int index = outCfg->index;
        Serial.print(F( "  Set servo" ));
        Serial.print( index );
        Serial.print(F( " pin=" ));
        Serial.print( pin );
        Serial.print(F( "->" ));
        Serial.println( pinValue );
        if (index == 1) {
          servo1.write( pinValue );
        }
        else if (index == 2) {
          servo2.write( pinValue );
        }
        else if (index == 3) {
          servo3.write( pinValue );
        }
        else if (index == 4) {
          servo4.write( pinValue );
        }
        break;
      }
      case OUTMODE_PWM: {
        Serial.print(F( "  Set PWM pin " ));
        Serial.print( pin );
        ledcWrite( outCfg->pwmChannel, pinValue );
        Serial.println( pinValue );
        break;
      }
      case OUTMODE_SWITCH: {
        Serial.print(F( "  Set port " ));
        Serial.print( pin );
        if (pinValue == 0) {
          digitalWrite( pin, LOW );
          Serial.println( " off" );
        }
        else if (pinValue == -1) {
          digitalWrite( pin, HIGH );
          Serial.println( " on" );
        }
        break;
      }
      default: { // PULSE or NONE
        if ((duration <= 0) || (duration > 500)) {
          duration = 500;
        }
        Serial.print(F( "  Pulse pin " ));
        Serial.print( pin );
        Serial.print( F(", val=") );
        Serial.print( pinValue );
        Serial.print( F(", len=") );
        Serial.println( duration );
        if (pinValue < 0) {
          digitalWrite( pin, HIGH );
        }
        else {
          Serial.print( F("doPulse NOT YET IMPLEMENTED for value=") );
          Serial.println( pinValue );
          //analogWrite( swPort, pinValue );
        }
        delay( duration );
        digitalWrite( pin, LOW );
      }
    }
  }
}

void doSendMsg( PortCfg* portCfg, const char* msg ) {
  int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
  if (index == 1) {
    HwSerial1.println( msg );
  }
  else if (index == 2) {
    HwSerial2.println( msg );
  }
}

//------------------------------------------------------------------
// Command Execution (doXXX, processXXX)
//------------------------------------------------------------------

void sendUdpMessage( const char *msg ) {
  IPAddress sendIP = WiFi.localIP();
  sendIP[3] = 255;
  Udp.beginPacket( sendIP, piRailReceivePort );
  Udp.print( msg );
  Udp.endPacket();
  udpOut[0] = 0; // clear udpOut buffer
}

void debugMsg( const char* msg, const QName* id, int val ) {
  Csv* csv = data->addCsv( CSVTYPE_DEBUGDATA );
  if (csv == NULL) { // buffer is full --> send
    sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
    data->clear();
    csv = data->addCsv( CSVTYPE_DEBUGDATA );
  }
  csv->appendStr( msg );
  if (id == NULL) {
    csv->appendStr( "null" );
    csv->appendStr( "null" );
  }
  else {
    csv->appendStr( id->getNamespace()->getUri() );
    csv->appendStr( id->getName() );
  }
  csv->append( val );
  sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
  data->clear();
}

void sendCmd( Cmd* cmd ) {
  xmlCmdWriter.begin( piRailFactory->NS_PIRAILFACTORY );
  cmd->writeXml( piRailFactory->ID_CMD, &xmlCmdWriter );
  const char* cmdMsg = xmlCmdWriter.getXml();
  sendUdpMessage( cmdMsg );
}
//Fahrbremshebel auslesen
  float floatMap(float X, float in_min, float in_max, float out_min, float out_max) //re-maps a number from one range to another
  {
    return (X - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }

void Hebel(){
   pinMode( 35, INPUT );
  int potiWert = 0;    // analog eingehender Wert
  int smoothWert = 0; //geglätteter Wert
//Problem nur mit den beiden hat der Geglättete Wert verspätung/verzögerung

//Ergänzung um verzögerung zu vermeiden
  int potiWertAlt = 0;
  int quickSmooth = 0;
  int differenz = 0;
  boolean quickAktiv = false;
  int x = 0;


  potiWert = analogRead(35); //PinG35

  smoothWert = 0.6 * smoothWert + 0.4 * analogRead(35); //smoothWert soll einen größeren Anteil haben (ca.60% bei größerem Zittern Wert erhöhen) und am ende muss 1 rauskommen

  //Ergänzung
  differenz = abs(potiWert - potiWertAlt); //Vergleiche Werte und setzte sie Absolut (immer ein positiver Wert)

  //Wenn meine differenz kein ist dann glättet er (wie oben)
  if (differenz <= 5 && quickAktiv == false) {
    quickSmooth = 0.6 * smoothWert + 0.4 * analogRead(35);
  }
    //wenn differnz groß ist dann arbeitet er ohne Glättung und er zählt mit (arbeitet dann 10 cycels ohne Glättung)
  else if (differenz <= 5 && quickAktiv == true){
    quickSmooth = analogRead(35);
    x++;
  }
  else {
    quickSmooth = analogRead(35);
    quickAktiv = true;
  };

  if (x >= 10) {
    x = 0;
    quickAktiv = false;
  };

  float voltage = floatMap(quickSmooth, 0, 4605, 0, 3.3);

  potiWertAlt = potiWert;
  //quickSmooth = quickSmooth / 10 * 3;
  Fahrhebel = quickSmooth;
 
  
  if (quickSmooth>690&&quickSmooth<1300) {
    gstufe= 1;
  }

  if (quickSmooth>1300&&quickSmooth<1700) {
    gstufe= 0;
  }

  if (quickSmooth>1700) {
    gstufe= -1;
  }
 // Serial.println(gstufe);
}

//==========================
// Main Loop
//==========================
void loop() {
  if ( resetMillisStart != 0L && (unsigned long)( millis() - resetMillisStart ) >= resetAfterMillis ) {
    Serial.println();
    Serial.print( F("Performing reset at ") );
    Serial.println( millis() );
    ESP.restart();
  }

  ArduinoOTA.handle();

  if (initialConfigMode) {
    if (!httpServerWasAccessed && millis() >= 3 * 60 * 1000) {
      // 3 minutes = 1 minute trying to connect to existing WiFi plus 2 minutes configuration window
      Serial.println();
      Serial.println( F("Config timeout. Restarting…") );
      Serial.println();
      ESP.restart();
    }
    return; // return even if it's not yet time to restart
  }

  if (xmlPiRail.isErrorMode()) {
    unsigned long currentTime = millis();
    long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      IPAddress sendIP = WiFi.localIP();
      sendIP[3] = 255;
      Udp.beginPacket( sendIP, piRailSendPort ); // funzt
      const char* deviceID = xmlPiRail.getDeviceID();
      if ((deviceID != NULL) && (strlen(deviceID) > 0)) {
        Udp.print( deviceID );
      }
      else {
        Udp.print( myHostname );
      }
      Udp.print( ":\n" );
      Udp.print( sketchNameAndCompileDate );
      Udp.print( "\nERROR - " );
      Udp.print( lastErrorMsg );
      Udp.endPacket();
      udpOut[0] = 0; // clear udpOut buffer
      xmlPiRail.setNextSyncTime( currentTime + 5000 );
    }
  }
  else {
    bool doSend = false;
    int packetSize = Udp.parsePacket();
    if (packetSize > 0) {
      uint32_t remoteIp = Udp.remoteIP();
      if ((locoIPAddr == 0) || (locoIPAddr = remoteIp)) {
        const QName *tag = NULL;
        xmlSyncReadBuffer[0] = 0;
        xmlSyncReader.initParser( xmlSyncReadBuffer, 2*UDP_BUF_SIZE, &Udp );
        tag = xmlSyncReader.nextTag( NULL );
        if (tag == piRailFactory->ID_STATE) {
          udpState.parse( &xmlSyncReader, tag );
          if (udpState.getId() == locoID) {
            locoIPAddr = remoteIp;

            MotorState* motorState = (MotorState*) udpState.firstChild( piRailFactory->ID_SP );
            if (motorState != NULL) {
              targetDir = motorState->getTgt();
              targetSpeed = motorState->getTgtI();
            }

            SetCmd* setCmd = (SetCmd*) locoLightCmd->firstChild( piRailFactory->ID_SET );
            if (locoCmdCount > 0) {
              locoCmdCount--;
            }
            else {
              if (setCmd->getValue() == '0') {
                setCmd->setValue( '1' );
              }
              else {
                setCmd->setValue( '0' );
              }
              locoCmdCount = 5;
              doSend = true;
            }
          }
          piRailFactory->clearState( &udpState );
        }
      }

      size_t readCount = Udp.available();
      while (readCount > 0) {
        if (readCount > UDP_BUF_SIZE) {
          readCount = UDP_BUF_SIZE;
        }
        Udp.readBytes( xmlSyncReadBuffer, readCount );
        readCount = Udp.available();
      }
    }
    //Aufruf Controlerdaten abfrage

    int newSpeed = 0;
    //Hebel();
    //newSpeed= gstufe*500;
    newSpeed= 500;
    //Serial.println(newSpeed);
    int newDir = DIR_FORWARD; // DIR_BACK
    if ((newSpeed != targetSpeed) || (newDir != targetDir)) {
      if (lastSend+200 < millis()) {
        MoCmd* moCmd = (MoCmd*) locoMotorCmd->firstChild( piRailFactory->ID_MO );
        moCmd->setDir( newDir );
        moCmd->setSpeed( newSpeed );
        printHeap( F("sendCmd speed") );
        sendCmd( locoMotorCmd );
        lastSend = millis();
      }  
    }

    if (doSend) {
      printHeap( F("sendCmd light") );    
     // sendCmd( locoLightCmd );
    }

    //---- Check if a input event has occurred
/*    Cfg* cfg = xmlPiRail.getCfg();
    IoCfg* ioCfg = xmlPiRail.getIoCfg();
    unsigned long evTime = millis();
    portENTER_CRITICAL( &mux );
    if (inputEventOccurred) {
      inputEventOccurred = false;
      SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
      while (sensorAction != NULL) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if ((inCfg != NULL) && (inCfg->eventOccurred)) {
          sensorAction->eventOccurred = true;
          inCfg->eventOccurred = false;
        }
        sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
      }
    }
    portEXIT_CRITICAL( &mux );

    //--- Process input events and polling
    SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
    while (sensorAction != NULL) {
      if (sensorAction->eventOccurred
          || ((sensorAction->nextPoll > 0) && (sensorAction->nextPoll < evTime))) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if (inCfg != NULL) {
          int pin = inCfg->getPin();
          if (pin > 0) {
            int value = digitalRead( pin );
            char charVal;
            if (value == HIGH) {
              charVal = '1';
            }
            else {
              charVal = '0';
            }
            if (sensorAction->processEvent( NULL, charVal, evTime, cfg, ioCfg )) {
              //sendState = true;
            }
          }
        }
      }
      sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
    }

    //--- Check timers and execute if time out
    TimerAction* timerAction = (TimerAction*) cfg->firstChild( piRailFactory->ID_TIMER );
    while (timerAction != NULL) {
      timerAction->doLoop( cfg, ioCfg );
      timerAction = (TimerAction*) timerAction->nextSibling( piRailFactory->ID_TIMER );
    }*/
  }
}
