#!/bin/bash

cd `dirname $0`

includes() {
  find $@ -name "*.h" | sed -e '
    /\/bootloaders\// d;
    /\/examples\// d;
    /\/tests\// d;
    /\/tools\// d;
    /\/variants\// d;
    s|/[^/]\+\.h$||;
    s|^|include_directories(|;
    s|$|)|
  ' | sort -u
}

cat << HEAD
cmake_minimum_required(VERSION 3.14)
project(sketchbook)

set(CMAKE_CXX_STANDARD 14)
HEAD

cat << ESP32_INCLUDES

# esp32 includes
ESP32_INCLUDES

includes ~/.arduino15/packages/esp32

cat << ESP8266_INCLUDES

# esp8266 includes
ESP8266_INCLUDES

includes ~/.arduino15/packages/esp8266

cat << SKETCHBOOK_INCLUDES

# sketchbook includes
SKETCHBOOK_INCLUDES

includes *

cat << SKETCHBOOK

# sketchbook executables
add_executable(sketchbook
SKETCHBOOK

find * -name "*.c*" -o -name "*.ino" | sed -e 's|^|        |' | sort
echo '        )'
