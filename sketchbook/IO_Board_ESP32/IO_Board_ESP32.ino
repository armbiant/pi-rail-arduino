/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <PString.h>

#include <EEPROM.h>
#include <FS.h>

#include <SPIFFS.h>

#include <WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <Servo.h>
#include <driver/adc.h>

#include <QName.h>
#include <XmlPiRail.h>
#include <PiHttpServer.h>
#include <PiOTA.h>

int INFO_LED = 2; //INFO_LED;

int LED_ON = HIGH;
int LED_OFF = LOW;

//----- Constants and variable for WiFi
const char *wifiModuleType = __FILE__;
int wifiStatus = WL_IDLE_STATUS;

Data* data = NULL;

#define HOSTNAME_SIZE 32
#define ERROR_MSG_SIZE 64
const int UDP_BUF_SIZE = 1500;

char inputString[UDP_BUF_SIZE + 1]; // a string to hold incoming data
char myHostname[HOSTNAME_SIZE + 1];
boolean stringComplete = false;        // whether the string is complete
int inputPos;

int nextPwmChannel = 0; // used for motor and PWM pins

WiFiUDP Udp;
char udpOut[UDP_BUF_SIZE+1];

HardwareSerial HwSerial1(1);
HardwareSerial HwSerial2(2);
Servo servo1;
Servo servo2;
int nextServo = 1;

const PROGMEM char *sketchFileNameAndCompileDate = __FILE__ " " __DATE__ " " __TIME__;
const char *sketchNameAndCompileDate;

bool initialConfigMode = false;
char lastErrorMsg[ERROR_MSG_SIZE + 1];

static PROGMEM const char *PI_SETUP = "Pi-Setup";

XmlPiRail xmlPiRail( wifiModuleType, 1024, UDP_BUF_SIZE );

PiHttpServer piHttpServer;
bool httpServerWasAccessed = false;

PiOTA piOta;

bool inputEventOccurred = false;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

//-------------------------------------------------
void IRAM_ATTR eventIsr(void *arg) {
  // Die ISR-Routine muss ohne fremden Code auskommen!
  // Der Grund:
  // Sowohl XmlPiRail als auch die Model-klassen haben NICHT das IRAM_ATTR, dadurch müssen
  // sie ggf. erst aus dem Flash-Speicher nachgeaden werden und dann dauert das zu lange
  // Symptom: Ich hatte gleich am Anfang von xmlPiRail.addEvent() ein Serial.println() drin
  // und das kam manchmal und manchmal nicht.
  portENTER_CRITICAL( &mux );
  InCfg *inCfg = static_cast<InCfg*>(arg);
  inputEventOccurred = true;
  inCfg->eventOccurred = true;
  portEXIT_CRITICAL( &mux );
}

/*  TODO Funktioniert teilweise nicht bei 2 Events (Dreiwegweiche) - schaltet zweiten nicht ab
struct timerAction {
  unsigned long millis;
  int interval;
  int pin;
  int value;
};

#define MAX_TIMER_ACTIONS 2

volatile int timerActionCount = 0;
struct timerAction timerActions[MAX_TIMER_ACTIONS];

bool addTimerAction( unsigned long millis, int len, const int port, int i );

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  if (timerActionCount > 0) {
    unsigned long now = millis();
    if( (unsigned long)( now - timerActions[0].millis ) >= (unsigned long)timerActions[0].interval ) {
      // set the pin
      digitalWrite(timerActions[0].pin, timerActions[0].value);

      if ( MAX_TIMER_ACTIONS > 1 ) {
        // memcpy overlaping areas from higher to lower address works.
        memcpy(timerActions, timerActions + sizeof(struct timerAction), ( MAX_TIMER_ACTIONS - 1 ) * sizeof(struct timerAction));
      }
      timerActionCount --;
    }
  }
  portEXIT_CRITICAL_ISR(&timerMux);
}

bool addTimerAction( const unsigned long millis, const int interval, const int pin, const int value ) {
  bool success = false;
  portENTER_CRITICAL_ISR(&timerMux);
  if (timerActionCount < MAX_TIMER_ACTIONS) {
    success = true;
    timerActions[timerActionCount].millis = millis;
    timerActions[timerActionCount].interval = interval;
    timerActions[timerActionCount].pin = pin;
    timerActions[timerActionCount].value = value;
    timerActionCount++;
  }
  portEXIT_CRITICAL_ISR(&timerMux);
  return success;
}
*/
//-------------------------------------------------
void fatalError( const __FlashStringHelper* msg ) {
  Serial.println( msg );
  Serial.println( F("Stopped - try to fix config") );
  xmlPiRail.setErrorMode( true );
  xmlPiRail.setNextSyncTime( millis() );
  int count = strlen_P( (PGM_P) msg );
  if (count > ERROR_MSG_SIZE) {
    count = ERROR_MSG_SIZE;
  }
  memcpy_P( lastErrorMsg, msg, count );
  lastErrorMsg[count] = 0;
}

unsigned long resetMillisStart = 0L;
unsigned long resetAfterMillis = 0L;

//-------------------------------------------------
void printHeap( const __FlashStringHelper* msg ) {
  Serial.print( F("ms=") );
  Serial.print( millis() );
  Serial.print( " " );
  Serial.print( msg );
  Serial.print( F(", heap=") );
  Serial.println( ESP.getFreeHeap() );
}

//-------------------------------------------------
const char* getVersion() {
  return __DATE__;
}

//-------------------------------------------------
int getHeapSize() {
  int heap = ESP.getFreeHeap();
  return heap;
}

//-------------------------------------------------
void doReset( long afterMillis ) {
  resetMillisStart = millis();
  resetAfterMillis = afterMillis > 0 ? afterMillis : 250; // have some minimum value
  Serial.println();
  Serial.print( F("Requested reset ") );
  Serial.print( afterMillis );
  Serial.print( F(" ms after ") );
  Serial.println( resetMillisStart );
}

//-------------------------------------------------
void httpServerAccessed() {
  httpServerWasAccessed = true;
}

int ledSpeedDefault = 250;

int ledSpeed = ledSpeedDefault;
int ledState = LED_ON;
unsigned long lastToggle = 0;

//-------------------------------------------------
bool toggleLed() {
  unsigned long now = millis();

  if (!lastToggle || now - lastToggle > ledSpeed) {
    ledState = ledState == LED_ON ? LED_OFF : LED_ON;
    digitalWrite( INFO_LED, ledState );
    lastToggle = now;
    return true;
  }
  else {
    return false;
  }
}

//-------------------------------------------------
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print( F("SSID: ") );
  Serial.println( WiFi.SSID() );

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print( F("IP: ") );
  Serial.print( ip.toString() );

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print( F(" RSSI: ") );
  Serial.print( rssi );
  Serial.println( F(" dBm") );
}

//-------------------------------------------------
boolean connectToWifi() {
  const char* ssid = xmlPiRail.getSsid();
  Serial.print( "ssid=" );  Serial.println( ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    return false;
  }
  const char* pass = xmlPiRail.getPass();

  //--- connect to Wifi
  Serial.print( F("Connecting to ") );
  Serial.print( ssid );
  WiFi.setHostname( myHostname );

  // Delay based on MAC to avoid all devices connecting same time
  Serial.print( F(", delay=") );
  uint8_t mac[6];
  WiFi.macAddress( mac );
  int waitConn = mac[5] * 4;
  Serial.println( waitConn );
  delay( waitConn );

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin( ssid, pass );

  ledSpeed = 200;
  unsigned long startMillis = millis();

  while (WiFi.status() != WL_CONNECTED && millis() - startMillis < 60 * 1000) {
    Serial.print(".");
    delay(200);
    toggleLed();
  }

  bool success = WiFi.status() == WL_CONNECTED;

  if (success) {
    Serial.println();
    Serial.println( F("Connected") );
    printWifiStatus();
  }
  else {
    Serial.println();
    Serial.println( F("WiFi failure") );
    WiFi.disconnect( true, false );
  }
  digitalWrite(INFO_LED, success ? LED_OFF : LED_ON);

  return success;
}

boolean isValidNameChar( char ch ) {
  return ( ((ch == '-') || (ch == '_'))
           || ((ch >= '0') && (ch <= '9'))
           || ((ch >= 'A') && (ch <= 'Z'))
           || ((ch >= 'a') && (ch <= 'z'))
         );
}

void setUpHostname( const char * deviceId, boolean macPrefix ) {
  memset( myHostname, 0, HOSTNAME_SIZE + 1 );
  int pos = 0;
  int i = 0;
  if (macPrefix) {
    String mac = WiFi.macAddress();
    while (pos < mac.length()) {
      char ch = mac.charAt(pos);
      if (isValidNameChar( mac[pos] )) {
        myHostname[i++] = ch;
      }
      pos++;
    }
    myHostname[i++] = '-';
  }
  if (deviceId != NULL) {
    pos = 0;
    while ((i < HOSTNAME_SIZE - 1) && (deviceId[pos] > 0)) {
      char ch = deviceId[pos];
      if (isValidNameChar( ch )) {
        myHostname[i++] = ch;
      }
      else {
        myHostname[i++] = '-';
      }
      pos++;
    }
  }
}

void setUpConfigWifi() {
  digitalWrite( INFO_LED, HIGH );
  Serial.println();
  Serial.print( F("Setup AP: ") );
  Serial.println( myHostname );

  WiFi.softAP( myHostname );

  IPAddress wifiLocalIp = WiFi.softAPIP();
  Serial.print( F("AP IP=") );
  Serial.println( wifiLocalIp );
}

//--------------------------
void initOutput( OutCfg* outCfg ) {
  int pin = outCfg->getPin();
  outType_t pinType = outCfg->getType();
  if (pin < 0x100) {
    switch (pinType) {
      case OUTTYPE_LOGIC:
      case OUTTYPE_HIGHSIDE:
      case OUTTYPE_LOWSIDE:
      case OUTTYPE_HALFBRIDGE: {
        pinMode( pin, OUTPUT );
        digitalWrite( pin, LOW );
        break;
      }
      case OUTTYPE_SERVO: {
        if (nextServo == 1) {
          servo1.attach( pin, nextPwmChannel );
        }
        else if (nextServo == 2) {
          servo2.attach( pin, nextPwmChannel );
        }
        else {
          Serial.println( F("ERROR too much servos!") );
          return;
        }
        outCfg->index = nextServo;
        outCfg->pwmChannel = nextPwmChannel;
        Serial.print( F("  Servo") );
        Serial.print( nextServo );
        Serial.print( F(", ch=") );
        Serial.println( nextPwmChannel );
        nextPwmChannel++;
        nextServo++;
        break;
      }
      default: {
        Serial.println( "unsupported pinType" );
      }
    }
  }
  else {
    Serial.println( F("--TODO-- init I2C port") );
  }
}

void initInput( InCfg* pinCfg ) {
  int pin = pinCfg->getPin();
  if (pinCfg->getPullup()) {
    pinMode( pin, INPUT_PULLUP );
  }
  else {
    pinMode( pin, INPUT );
  }
}

bool attachOutPin( OutCfg* outCfg, outMode_t outMode ) {
  ExtCfg *extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    if (outCfg->attached == OUTMODE_NONE) {
      if (outMode == OUTMODE_PWM) {
        if (outCfg->pwmChannel == -1) {
          int pin = outCfg->getPin();
          pinMode( pin, OUTPUT );
          outCfg->pwmChannel = nextPwmChannel;
          ledcSetup( nextPwmChannel, 5000, 10 );
          ledcAttachPin( pin, nextPwmChannel );
          Serial.print(F( "  PWM, ch=" ));
          Serial.println( nextPwmChannel );
          nextPwmChannel++;
        }
      }
      outCfg->attached = outMode;
      return true;
    }
    else {
      return (outMode == outCfg->attached);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (outCfg->attached == OUTMODE_NONE) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return (outCfg->attached == outMode);
      }
    }
  }
}

bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useIterrupt ) {
  if (inCfg != NULL) {
    if (inCfg->attached == INMODETYPE_NONE) {
      if (inMode == INMODETYPE_DIGITAL) {
        int pin = inCfg->getPin();
        if (pin > 0) {
          if (useIterrupt) {
            switch (fireType) {
              case FIRETYPE_ONFALL: {
                attachInterruptArg( pin, eventIsr, inCfg, FALLING );
                break;
              }
              case FIRETYPE_ONRISE: {
                attachInterruptArg( pin, eventIsr, inCfg, RISING );
                break;
              }
              default: {
                attachInterruptArg( pin, eventIsr, inCfg, CHANGE );
              }
            }
            Serial.print( "Attached interrupt to pin=" );
            Serial.println( pin );
          }
          else{
            Serial.print( "Activated input polling for pin=" );
            Serial.println( pin );
          }
        }
        else {
          Serial.print( "Pin missing in inCfg " );
          xmlPiRail.serialPrintlnQName( inCfg->getId() );
        }
      }
      inCfg->attached = inMode;
      return true;
    }
    else {
      return (inCfg->attached == inMode);
    }
  }
}

bool attachPort( PortCfg* portCfg, portMode_t portMode ) {
  ExtCfg *extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_NONE) {
      if (portCfg->getType() == PORTTYPE_SERIALMODULATED) {
        int pwmPin = portCfg->getPinByType( PORTPINTYPE_CLOCKPIN );
        Serial.print("  pwm=");
        Serial.print( pwmPin );
        int rxPin = portCfg->getPinByType( PORTPINTYPE_RX_PIN );
        Serial.print(", rx=");
        Serial.print(rxPin );
        int txPin = portCfg->getPinByType( PORTPINTYPE_TX_PIN );
        Serial.print(", tx=");
        Serial.print( txPin );
        int baudRate = portCfg->getParamIntVal( piRailFactory->ID_BAUD_RATE );
        Serial.print(", baud=");
        Serial.print( baudRate );
        int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
        if (index == 1) {
          Serial.print( F(", port=1...") );
          HwSerial1.begin( baudRate, SERIAL_8N1, rxPin, txPin );
          Serial.println( F("ok") );
        }
        else if (index == 2) {
          Serial.print( F(", port=2...") );
          HwSerial2.begin( baudRate, SERIAL_8N1, rxPin, txPin );
          Serial.println( F("ok") );
        }
        else {
          Serial.print( F("Invalid msg index (1..2): ") );
          Serial.println( index );
          return false;
        }
        int pwmChannel = nextPwmChannel++;
        ledcSetup( pwmChannel, 38000, 8 ); // 38 kHz PWM, 8-bit resolution
        ledcAttachPin( pwmPin, pwmChannel ); // assign a led pins to a channel
        ledcWrite( pwmChannel, 100 );
        portCfg->attached = portMode;
        return true;
      }
      else {
        Serial.println( F("Unsupported port type") );
        return false;
      }
    }
    else {
      return (portCfg->attached == portMode);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (portCfg->attached == PORTMODE_NONE) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return (portCfg->attached == portMode);
      }
    }
  }
}

//--------------------------
void setup() {
  sketchNameAndCompileDate = strrchr( sketchFileNameAndCompileDate, '/' );
  if (sketchNameAndCompileDate) {
    sketchNameAndCompileDate ++;
  }
  else {
    sketchNameAndCompileDate = sketchFileNameAndCompileDate;
  }

  pinMode(INFO_LED, OUTPUT);     // Initialize the INFO_LED pin as an output
  digitalWrite(INFO_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  lastErrorMsg[0] = 0;

  Serial.begin( 115200 );
  inputPos = 0;
  Serial.println();
  Serial.println( sketchNameAndCompileDate );
  Serial.println();
  printHeap( F("Begin setup") );

  piRailFactory->max_csv_count = 5;
  piRailFactory->max_ev_count = 4;
  piRailFactory->max_fileDef_count = 10;

  //--- From https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html :
  // Please do not use the interrupt of GPIO36 and GPIO39 when using ADC or Wi-Fi with sleep mode enabled.
  // Please refer to the comments of adc1_get_raw. Please refer to section 3.11 of 
  // ‘ECO_and_Workarounds_for_Bugs_in_ESP32’ for the description of this issue. 
  // As a workaround, call adc_power_acquire() in the app. This will result in higher power consumption
  // (by ~1mA), but will remove the glitches on GPIO36 and GPIO39.
  adc_power_acquire();

  data = new Data();
  xmlPiRail.setup( WiFi.macAddress().c_str(), DEVICETYPE_SWITCHBOX );

  // Ensure we have a hostname before connectToWifi()
  Cfg* cfg = xmlPiRail.getCfg();
  const char *deviceName;
  if ((cfg != NULL) && (cfg->getId() != NULL) && (strlen(cfg->getId()->getName()) > 0)) {
    deviceName = cfg->getId()->getName();
    setUpHostname( deviceName, false );
  }
  else {
    deviceName = xmlPiRail.getDeviceID();
    if (deviceName == NULL) {
      deviceName = "INIT";
    }
    setUpHostname( deviceName, true );
  }

  if (!initialConfigMode) {
    initialConfigMode = !connectToWifi();
  }

  if (!initialConfigMode && !xmlPiRail.isErrorMode()) {
    Serial.print( F("Start listen UDP ") );
    Serial.println( piRailReceivePort );
    // if you get a connection, report back via serial:
    Udp.begin( piRailReceivePort );
    xmlPiRail.begin( udpOut, -1 );
  }

  if (initialConfigMode) {
    // discard the hostname and rebuild
    setUpHostname( deviceName, true );
  }

  Serial.print( F("Hostname=") );
  Serial.println( myHostname );

  if (initialConfigMode) {
    setUpConfigWifi();
  }

  delay(250);

  FileCfg* fileCfg = xmlPiRail.getFileCfg();
  NetCfg* netCfg = xmlPiRail.getNetCfg();
  piHttpServer.init( sketchNameAndCompileDate, myHostname, fileCfg, netCfg );
  piHttpServer.begin();

  piOta.begin( myHostname );

  // Use 1st timer of 4 (counted from zero).
/* TODO  - Funktioniert nicht (s.o.)
  timer = timerBegin(0, 80, true);

  // Attach onTimer function to our timer.
  timerAttachInterrupt(timer, &onTimer, true);

  // Set alarm to call onTimer function every 100 microseconds.
  timerAlarmWrite(timer, 100 * 1000, true);

  // Start an alarm
  timerAlarmEnable(timer);
*/
  printHeap( F("Finished setup") );
}

//------------------------------------------------------------------
// Hardware access functions
//------------------------------------------------------------------

int readMotorSensor( PortCfg* portCfg, int currentSpeed ) {
  // not supported
  return 0;
}

void processMsgState( MsgState* newMsgState ) {
  // not supported
}

const QName* checkIDReader( SensorAction* sensorAction ) {
  // not supported
  return NULL;
}

void doMotor( PortCfg* portCfg, char dir, int motorOutput ) {
  // not supported
}

void doOutputPin( OutCfg* outCfg, int pinValue, int duration ) {
  if (outCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    int pin = outCfg->getPin();
    if (pin < 0) {
      return;
    }
    outMode_t outMode = outCfg->attached;
    switch (outMode) {
      case OUTMODE_SERVO: {
        int index = outCfg->index;
        Serial.print(F( "  Set servo" ));
        Serial.print( index );
        Serial.print(F( " pin=" ));
        Serial.print( pin );
        Serial.print(F( "->" ));
        Serial.println( pinValue );
        if (index == 1) {
          servo1.write( pinValue );
        }
        else if (index == 2) {
          servo2.write( pinValue );
        }
        break;
      }
      case OUTMODE_PWM: {
        Serial.print(F( "  Set PWM pin " ));
        Serial.print( pin );
        ledcWrite( outCfg->pwmChannel, pinValue );
        Serial.println( pinValue );
        break;
      }
      case OUTMODE_SWITCH: {
        Serial.print(F( "  Set port " ));
        Serial.print( pin );
        if (pinValue == 0) {
          digitalWrite( pin, LOW );
          Serial.println( " off" );
        }
        else if (pinValue == -1) {
          digitalWrite( pin, HIGH );
          Serial.println( " on" );
        }
        break;
      }
      default: { // PULSE or NONE
        if ((duration <= 0) || (duration > 500)) {
          duration = 500;
        }
        Serial.print(F( "  Pulse pin " ));
        Serial.print( pin );
        Serial.print( F(", val=") );
        Serial.print( pinValue );
        Serial.print( F(", len=") );
        Serial.println( duration );
        if (pinValue < 0) {
          digitalWrite( pin, HIGH );
        }
        else {
          Serial.print( F("doPulse NOT YET IMPLEMENTED for value=") );
          Serial.println( pinValue );
          //analogWrite( swPort, pinValue );
        }
        delay( duration );
        digitalWrite( pin, LOW );
      }
    }
  }
/*  else if (swPort < 0x1000) { // 8bit chip via I2C
    int chipNum = ((swPort & 0x0F00) >> 8) - 1;
    Serial.print("Switch chip num=");
    Serial.print( chipNum );
    int portNum = (swPort & 0x0F);
    Serial.print(",port=");
    Serial.println( portNum );
    if (chipNum < I2C_CHIP_COUNT) {
      Wire.beginTransmission( i2c_chip_addr[chipNum] );
      Wire.write( 0x01 << portNum );
      Wire.endTransmission();
      delay( switch_time );
      Wire.beginTransmission( i2c_chip_addr[chipNum] );
      Wire.write( 0x00 );
      Wire.endTransmission();
    }
  }
  else { // 16Bit MC23017 via I2C
    int chipNum = ((swPort & 0x0F00) >> 8) - 1;
    Serial.print("Switch mc23017 num=");
    Serial.print( chipNum );
    int portNum = (swPort & 0x0F);
    Serial.print(",port=");
    Serial.println( portNum );
    if (chipNum < I2C_MC23017_COUNT) {
      int portReg;
      if (portNum < 7) {
        portReg = MC23017_GPIOA;
      }
      else {
        portReg = MC23017_GPIOB;
        portNum = portNum - 8;
      }
      Wire.beginTransmission( i2c_mc23017_addr[chipNum] );
      Wire.write( portReg );
      Wire.write( 0x01 << portNum );
      Wire.endTransmission();
      delay( switch_time );
      Wire.beginTransmission( i2c_mc23017_addr[chipNum] );
      Wire.write( portReg );
      Wire.write( 0 );
      Wire.endTransmission();
    }
  }*/
}

void doSendMsg( PortCfg* portCfg, const char* msg ) {
  int index = portCfg->getParamIntVal( piRailFactory->ID_INDEX );
  if (index == 1) {
    HwSerial1.println( msg );
  }
  else if (index == 2) {
    HwSerial2.println( msg );
  }
}

//------------------------------------------------------------------
// Command Execution (doXXX, processXXX)
//------------------------------------------------------------------

void sendUdpMessage( const char *msg ) {
  //IPAddress sendIP;
  //WiFi.hostByName(udpSendIP, sendIP);
  //Udp.beginPacketMulticast( WiFi.localIP(), sendIP, piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast( sendIP, piRailSendPort, WiFi.localIP() ); // funzt nicht
  //Udp.beginPacket( "192.168.43.50", piRailSendPort ); // funzt
  //Udp.beginPacket( "255.255.255.255", piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast(addr, port, WiFi.localIP())
  IPAddress sendIP = WiFi.localIP();
  sendIP[3] = 255;
  Udp.beginPacket( sendIP, piRailSendPort ); // funzt
  Udp.print( msg );
  Udp.endPacket();
  udpOut[0] = 0; // clear udpOut buffer
}

void debugMsg( const char* msg, const QName* id, int val ) {
  Csv* csv = data->addCsv( CSVTYPE_DEBUGDATA );
  if (csv == NULL) { // buffer is full --> send
    sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
    data->clear();
    csv = data->addCsv( CSVTYPE_DEBUGDATA );
  }
  csv->appendStr( msg );
  if (id == NULL) {
    csv->appendStr( "null" );
    csv->appendStr( "null" );
  }
  else {
    csv->appendStr( id->getNamespace()->getUri() );
    csv->appendStr( id->getName() );
  }
  csv->append( val );
  sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
  data->clear();
}

//==========================
// Main Loop
//==========================
void loop() {
  if ( resetMillisStart != 0L && (unsigned long)( millis() - resetMillisStart ) >= resetAfterMillis ) {
    Serial.println();
    Serial.print( F("Performing reset at ") );
    Serial.println( millis() );
    ESP.restart();
  }

  ArduinoOTA.handle();

  if (initialConfigMode) {
    if (!httpServerWasAccessed && millis() >= 3 * 60 * 1000) {
      // 3 minutes = 1 minute trying to connect to existing WiFi plus 2 minutes configuration window
      Serial.println();
      Serial.println( F("Config timeout. Restarting…") );
      Serial.println();
      ESP.restart();
    }
    return; // return even if it's not yet time to restart
  }

  if (xmlPiRail.isErrorMode()) {
    unsigned long currentTime = millis();
    long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      IPAddress sendIP = WiFi.localIP();
      sendIP[3] = 255;
      Udp.beginPacket( sendIP, piRailSendPort ); // funzt
      const char* deviceID = xmlPiRail.getDeviceID();
      if ((deviceID != NULL) && (strlen(deviceID) > 0)) {
        Udp.print( deviceID );
      }
      else {
        Udp.print( myHostname );
      }
      Udp.print( ":\n" );
      Udp.print( sketchNameAndCompileDate );
      Udp.print( "\nERROR - " );
      Udp.print( lastErrorMsg );
      Udp.endPacket();
      udpOut[0] = 0; // clear udpOut buffer
      xmlPiRail.setNextSyncTime( currentTime + 5000 );
    }
  }
  else {
    bool sendState = false;
    int packetSize = Udp.parsePacket();
    if (packetSize > 0) {
      IPAddress remoteIp = Udp.remoteIP();
      sendState = xmlPiRail.processXml( WiFi.localIP(), remoteIp, &Udp );
    }

    //---- Check if a input event has occurred
    Cfg* cfg = xmlPiRail.getCfg();
    IoCfg* ioCfg = xmlPiRail.getIoCfg();
    unsigned long evTime = millis();
    portENTER_CRITICAL( &mux );
    if (inputEventOccurred) {
      inputEventOccurred = false;
      SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
      while (sensorAction != NULL) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if ((inCfg != NULL) && (inCfg->eventOccurred)) {
          sensorAction->eventOccurred = true;
          inCfg->eventOccurred = false;
        }
        sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
      }
    }
    portEXIT_CRITICAL( &mux );

    //--- Process input events and polling
    SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
    while (sensorAction != NULL) {
      if (sensorAction->eventOccurred
          || ((sensorAction->nextPoll > 0) && (sensorAction->nextPoll < evTime))) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if (inCfg != NULL) {
          int pin = inCfg->getPin();
          if (pin > 0) {
            int value = digitalRead( pin );
            char charVal;
            if (value == HIGH) {
              charVal = '1';
            }
            else {
              charVal = '0';
            }
            if (sensorAction->processEvent( NULL, charVal, evTime, cfg, ioCfg )) {
              sendState = true;
            }
          }
        }
      }
      sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
    }

    //--- Send sync message
    unsigned long currentTime = millis();
    unsigned long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      sendState = true;
      xmlPiRail.setNextSyncTime( -1 );
    }
    if (sendState) {
      // print events and speed changes into udpOut
      xmlPiRail.sendDeviceState( currentTime, WiFi.RSSI() );
    }

    //--- Check timers and execute if time out
    TimerAction* timerAction = (TimerAction*) cfg->firstChild( piRailFactory->ID_TIMER );
    while (timerAction != NULL) {
      timerAction->doLoop( cfg, ioCfg );
      timerAction = (TimerAction*) timerAction->nextSibling( piRailFactory->ID_TIMER );
    }

    //--- Check triggers
    xmlPiRail.processState( WiFi.localIP(), WiFi.localIP(), xmlPiRail.getDeviceState() );
  }
}
