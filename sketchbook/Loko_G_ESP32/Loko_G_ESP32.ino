/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <PString.h>

#include <EEPROM.h>
#include <FS.h>

#include <SPIFFS.h>

#include <WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <Servo.h>
#include <driver/adc.h>

#include <XmlPiRail.h>
#include <PiHttpServer.h>
#include <PiOTA.h>

#include <DCCPacketScheduler.h>

#include <PN532_HSU.h>
#include <PN532.h>

int INFO_LED = 2; //INFO_LED;

int LED_ON = HIGH;
int LED_OFF = LOW;

//----- Constants and variable for WiFi
const char *wifiModuleType = __FILE__;
int wifiStatus = WL_IDLE_STATUS;

//----- Constants and variable for Motor
const int LOKO_MODE_DIRECT = 0;
const int LOKO_MODE_DCC = 1;
const int LOKO_MODE_MM2 = 2;
int lokoMode = LOKO_MODE_DIRECT;
Data* data = NULL;

//----- Constants and variable for ID-Reader
bool serialChangedState = false;
int idReaderPos = -1;
char idReaderData[MSG_MAX_LEN + 1];
long idReadMillis = 0;
const int MAX_MSG_STATE = 100;
MsgState msgStateList[MAX_MSG_STATE];
boolean rfidPN532 = false;
uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

#define HOSTNAME_SIZE 32
#define ERROR_MSG_SIZE 64
const int UDP_BUF_SIZE = 1500;

//char inputString[UDP_BUF_SIZE + 1]; // a string to hold incoming data
char myHostname[HOSTNAME_SIZE + 1];
boolean stringComplete = false;        // whether the string is complete
int inputPos;

int nextPwmChannel = 0; // used for motor and PWM pins

WiFiUDP Udp;
char udpOut[UDP_BUF_SIZE+1];

HardwareSerial HwSerial1(1);
PN532_HSU pn532hsu( HwSerial1, 21, 22 );
PN532 nfc( pn532hsu );
//HardwareSerial HwSerial2(2);

Servo servo1;
Servo servo2;
int nextServo = 1;

const PROGMEM char *sketchFileNameAndCompileDate = __FILE__ " " __DATE__ " " __TIME__;
const char *sketchNameAndCompileDate;

bool initialConfigMode = false;
char lastErrorMsg[ERROR_MSG_SIZE + 1];

static PROGMEM const char *PI_SETUP = "Pi-Setup";

XmlPiRail xmlPiRail( wifiModuleType, 1024, UDP_BUF_SIZE );

PiHttpServer piHttpServer;
bool httpServerWasAccessed = false;

PiOTA piOta;

bool inputEventOccurred = false;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

DCCPacketScheduler dps;
bool dpsActive = false;
extern volatile bool get_next_packet;
extern volatile uint8_t current_packet[6];

//-------------------------------------------------
void IRAM_ATTR eventIsr(void *arg) {
  // Die ISR-Routine muss ohne fremden Code auskommen!
  // Der Grund:
  // Sowohl XmlPiRail als auch die Model-klassen haben NICHT das IRAM_ATTR, dadurch müssen
  // sie ggf. erst aus dem Flash-Speicher nachgeaden werden und dann dauert das zu lange
  // Symptom: Ich hatte gleich am Anfang von xmlPiRail.addEvent() ein Serial.println() drin
  // und das kam manchmal und manchmal nicht.
  portENTER_CRITICAL( &mux );
  InCfg *inCfg = static_cast<InCfg*>(arg);
  inputEventOccurred = true;
  inCfg->eventOccurred = true;
  portEXIT_CRITICAL( &mux );
}

//-------------------------------------------------
void fatalError( const __FlashStringHelper* msg ) {
  Serial.println( msg );
  Serial.println( F("Stopped - try to fix config") );
  xmlPiRail.setErrorMode( true );
  xmlPiRail.setNextSyncTime( millis() );
  int count = strlen_P( (PGM_P) msg );
  if (count > ERROR_MSG_SIZE) {
    count = ERROR_MSG_SIZE;
  }
  memcpy_P( lastErrorMsg, msg, count );
  lastErrorMsg[count] = 0;
}

unsigned long resetMillisStart = 0L;
unsigned long resetAfterMillis = 0L;

//-------------------------------------------------
void printHeap( const __FlashStringHelper* msg ) {
  Serial.print( F("ms=") );
  Serial.print( millis() );
  Serial.print( " " );
  Serial.print( msg );
  Serial.print( F(", heap=") );
  Serial.println( ESP.getFreeHeap() );
}

//-------------------------------------------------
const char* getVersion() {
  return __DATE__;
}

//-------------------------------------------------
int getHeapSize() {
  int heap = ESP.getFreeHeap();
  return heap;
}

//-------------------------------------------------
void doReset( long afterMillis ) {
  resetMillisStart = millis();
  resetAfterMillis = afterMillis > 0 ? afterMillis : 250; // have some minimum value
  Serial.println();
  Serial.print( F("Requested reset ") );
  Serial.print( afterMillis );
  Serial.print( F(" ms after ") );
  Serial.println( resetMillisStart );
}

//-------------------------------------------------
void httpServerAccessed() {
  httpServerWasAccessed = true;
}

int ledSpeedDefault = 250;

int ledSpeed = ledSpeedDefault;
int ledState = LED_ON;
unsigned long lastToggle = 0;

//-------------------------------------------------
bool toggleLed() {
  unsigned long now = millis();

  if (!lastToggle || now - lastToggle > ledSpeed) {
    ledState = ledState == LED_ON ? LED_OFF : LED_ON;
    digitalWrite( INFO_LED, ledState );
    lastToggle = now;
    return true;
  }
  else {
    return false;
  }
}

//-------------------------------------------------
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print( F("SSID: ") );
  Serial.println( WiFi.SSID() );

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print( F("IP: ") );
  Serial.print( ip.toString() );

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print( F(" RSSI: ") );
  Serial.print( rssi );
  Serial.println( F(" dBm") );
}

//-------------------------------------------------
boolean connectToWifi() {
  const char* ssid = xmlPiRail.getSsid();
  Serial.print( "ssid=" );  Serial.println( ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    return false;
  }
  const char* pass = xmlPiRail.getPass();

  //--- connect to Wifi
  Serial.print( F("Connecting to ") );
  Serial.print( ssid );
  WiFi.setHostname( myHostname );

  // Delay based on MAC to avoid all devices connecting same time
  Serial.print( F(", delay=") );
  uint8_t mac[6];
  WiFi.macAddress( mac );
  int waitConn = mac[5] * 4;
  Serial.println( waitConn );
  delay( waitConn );

  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  WiFi.begin( ssid, pass );
  //connect2nearestAP();

  ledSpeed = 200;
  unsigned long startMillis = millis();

  while (WiFi.status() != WL_CONNECTED && millis() - startMillis < 60 * 1000) {
    Serial.print(".");
    delay(200);
    toggleLed();
  }

  bool success = WiFi.status() == WL_CONNECTED;

  if (success) {
    Serial.println();
    Serial.println( F("Connected") );
    printWifiStatus();
  }
  else {
    Serial.println();
    Serial.println( F("WiFi failure") );
    WiFi.disconnect( true, false );
  }
  digitalWrite(INFO_LED, success ? LED_OFF : LED_ON);

  return success;
}

#define SaveDisconnectTime 1000 // Time im ms for save disconnection, needed to avoid that WiFi works only evey secon boot: https://github.com/espressif/arduino-esp32/issues/2501
#define WiFiTime 10    // Max time in s for successful WiFi connection.
#define WiFiOK 0       // WiFi connected
#define WiFiTimedOut 1 // WiFi connection timed out
#define WiFiNoSSID 2   // The SSID was not found during network scan
#define WiFiRSSI -75   // Min. required signal strength for a good WiFi cennection. If lower, new scan is initiated in checkWiFi()

/**
 * Disconnect from WiFi, scan the network for the strongest AP at the defined SSID and try to connect.
 *
 * @return WiFiOK, WiFiTimedOut, or WiFiNoSSID
 */
int connect2nearestAP()
{
  int n;
  int i = 0;

  if (WiFi.status() == WL_CONNECTED) {
    // the following awkward code seems to work to avoid the following bug in ESP32 WiFi connection:
    // https://github.com/espressif/arduino-esp32/issues/2501
    WiFi.disconnect( true ); // delete old config
    WiFi.persistent( false ); //Avoid to store Wifi configuration in Flash
    WiFi.mode( WIFI_OFF );
    WiFi.mode( WIFI_STA );
    WiFi.disconnect( true ); // delete old config
    //WiFi.setHostname( HostName );
    WiFi.begin();
    delay( SaveDisconnectTime ); // 500ms seems to work in most cases, may depend on AP
    WiFi.disconnect( true ); // delete old config
  }

  Serial.println("scan start");
  n = WiFi.scanNetworks(); // WiFi.scanNetworks will return the number of networks found
  Serial.println("scan done");

  if (n == 0) {
    Serial.println("no networks found");
  }
  else {
 /*   Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i) {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print("BSSID: ");
      Serial.print(WiFi.BSSIDstr(i));
      Serial.print("  ");
      Serial.print(WiFi.RSSI(i));
      Serial.print("dBm, ");
      Serial.print(constrain(2 * (WiFi.RSSI(i) + 100), 0, 100));
      Serial.print("% ");
      Serial.print((WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? "open       " : "encrypted  ");
      Serial.println(WiFi.SSID(i));
      delay(10);
    }*/
  }
  //Serial.println();

  const char* ssid = xmlPiRail.getSsid();
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    return false;
  }
  const char* pass = xmlPiRail.getPass();
  //  while ( !strcmp(ssid, &WiFi.SSID(i)) && (i < n)) {
  while ( String(ssid) != String(WiFi.SSID(i)) && (i < n)) {
    i++;
  }

  if (i == n) {
    Serial.print("No network with SSID ");
    Serial.println(ssid);
    Serial.print(" found!");
    return WiFiNoSSID;
  }

  Serial.print("SSID match found at: ");
  Serial.println(i + 1);

  //WiFi.setHostname(HostName);
  WiFi.begin(ssid, pass, 0, WiFi.BSSID(i));
  i = 0;
  Serial.print("Connecting ");
  while ((WiFi.status() != WL_CONNECTED) && (i < WiFiTime * 10)) {
    delay(100);
    Serial.print(".");
    i++;
  }

  Serial.println();

  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Connection to ");
    Serial.print(ssid);
    Serial.println(" timed out! ");
    return WiFiTimedOut;
  }

  Serial.print("Connected to BSSID: ");
  Serial.print(WiFi.BSSIDstr());
  Serial.print("  ");
  Serial.print(WiFi.RSSI());
  Serial.print("dBm, ");
  Serial.print(constrain(2 * (WiFi.RSSI() + 100), 0, 100));
  Serial.print("% ");
  Serial.print(WiFi.SSID());
  Serial.print(" IP: ");
  Serial.println(WiFi.localIP());

  return (WiFiOK);
}

/**
 * Check the signal strength of the connection - if too low, reconnect to the strongest AP;
 * good if the ESP is moving around and reconnection to the nearest AP makes sense
 */
void checkWiFi() {
  if (WiFi.status() == WL_CONNECTED) {
    if ((WiFi.RSSI() > WiFiRSSI) && (WiFi.RSSI() < -25)) { // good connection
      return;
    }
    Serial.print("Connection bad: ");
    Serial.print(WiFi.RSSI());
    Serial.print("dBm, ");
    Serial.print(constrain(2 * (WiFi.RSSI() + 100), 0, 100));
    Serial.print("% ");
    Serial.println(" ... try reconnect");
  }
  else {
    Serial.print( "Connection lost ... try reconnect" );
  }
  connect2nearestAP();
  /*const char* ssid = xmlPiRail.getSsid();
  Serial.print( "ssid=" );  Serial.println( ssid );
  if ((ssid == NULL) || (strlen(ssid) <= 0)) {
    return;
  }
  const char* pass = xmlPiRail.getPass();
  WiFi.begin( ssid, pass );
  */
}

boolean isValidNameChar( char ch ) {
  return ( ((ch == '-') || (ch == '_'))
           || ((ch >= '0') && (ch <= '9'))
           || ((ch >= 'A') && (ch <= 'Z'))
           || ((ch >= 'a') && (ch <= 'z'))
         );
}

void setUpHostname( const char * deviceId, boolean macPrefix ) {
  memset( myHostname, 0, HOSTNAME_SIZE + 1 );
  int pos = 0;
  int i = 0;
  if (macPrefix) {
    String mac = WiFi.macAddress();
    while (pos < mac.length()) {
      char ch = mac.charAt(pos);
      if (isValidNameChar( mac[pos] )) {
        myHostname[i++] = ch;
      }
      pos++;
    }
    myHostname[i++] = '-';
  }
  if (deviceId != NULL) {
    pos = 0;
    while ((i < HOSTNAME_SIZE - 1) && (deviceId[pos] > 0)) {
      char ch = deviceId[pos];
      if (isValidNameChar( ch )) {
        myHostname[i++] = ch;
      }
      else {
        myHostname[i++] = '-';
      }
      pos++;
    }
  }
}

void setUpConfigWifi() {
  digitalWrite( INFO_LED, HIGH );
  Serial.println();
  Serial.print( F("Setup AP: ") );
  Serial.println( myHostname );

  WiFi.softAP( myHostname );

  IPAddress wifiLocalIp = WiFi.softAPIP();
  Serial.print( F("AP IP=") );
  Serial.println( wifiLocalIp );
}

adc1_channel_t getADChannel( int pin ) {
  switch( pin ) {
    case 36 : return ADC1_CHANNEL_0;
    case 37 : return ADC1_CHANNEL_1;
    case 38 : return ADC1_CHANNEL_2;
    case 39 : return ADC1_CHANNEL_3;
    case 32 : return ADC1_CHANNEL_4;
    case 33 : return ADC1_CHANNEL_5;
    case 34 : return ADC1_CHANNEL_6;
    case 35 : return ADC1_CHANNEL_7;
  }
}

//--------------------------
void initOutput( OutCfg* outCfg ) {
  int pin = outCfg->getPin();
  outType_t pinType = outCfg->getType();
  if (pin < 0x100) {
    switch (pinType) {
      case OUTTYPE_LOGIC:
      case OUTTYPE_HIGHSIDE:
      case OUTTYPE_LOWSIDE:
      case OUTTYPE_HALFBRIDGE: {
        pinMode( pin, OUTPUT );
        digitalWrite( pin, LOW );
        break;
      }
      case OUTTYPE_SERVO: {
        if (nextServo == 1) {
          servo1.attach( pin, nextPwmChannel );
        }
        else if (nextServo == 2) {
          servo2.attach( pin, nextPwmChannel );
        }
        else {
          Serial.println( F("ERROR too much servos!") );
          return;
        }
        outCfg->index = nextServo;
        outCfg->pwmChannel = nextPwmChannel;
        Serial.print( F("  Servo") );
        Serial.print( nextServo );
        Serial.print( F(", ch=") );
        Serial.println( nextPwmChannel );
        nextPwmChannel++;
        nextServo++;
        break;
      }
      default: {
        Serial.println( "unsupported pinType" );
      }
    }
  }
  else {
    Serial.println( F("--TODO-- init I2C port") );
  }
}

void initInput( InCfg* pinCfg ) {
  int pin = pinCfg->getPin();
  if (pinCfg->getPullup()) {
    pinMode( pin, INPUT_PULLUP );
  }
  else {
    pinMode( pin, INPUT );
  }
}

bool attachOutPin( OutCfg* outCfg, outMode_t outMode ) {
  ExtCfg *extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    if (outCfg->attached == OUTMODE_NONE) {
      if (outMode == OUTMODE_PWM) {
        if (outCfg->pwmChannel == -1) {
          int pin = outCfg->getPin();
          pinMode( pin, OUTPUT );
          outCfg->pwmChannel = nextPwmChannel;
          ledcSetup( nextPwmChannel, 5000, 10 );
          ledcAttachPin( pin, nextPwmChannel );
          Serial.print(F( "  PWM, ch=" ));
          Serial.println( nextPwmChannel );
          nextPwmChannel++;
        }
      }
      outCfg->attached = outMode;
      return true;
    }
    else {
      return (outMode == outCfg->attached);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (outCfg->attached == OUTMODE_NONE) {
        outCfg->attached = outMode;
        return true;
      }
      else {
        return (outCfg->attached == outMode);
      }
    }
  }
}

bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useIterrupt ) {
  if (inCfg != NULL) {
    if (inCfg->attached == INMODETYPE_NONE) {
      if (inMode == INMODETYPE_DIGITAL) {
        int pin = inCfg->getPin();
        if (pin > 0) {
          if (useIterrupt) {
            switch (fireType) {
              case FIRETYPE_ONFALL: {
                attachInterruptArg( pin, eventIsr, inCfg, FALLING );
                break;
              }
              case FIRETYPE_ONRISE: {
                attachInterruptArg( pin, eventIsr, inCfg, RISING );
                break;
              }
              default: {
                attachInterruptArg( pin, eventIsr, inCfg, CHANGE );
              }
            }
            Serial.print( "Attached interrupt to pin=" );
            Serial.println( pin );
          }
          else{
            Serial.print( "Activated input polling for pin=" );
            Serial.println( pin );
          }
        }
        else {
          Serial.print( "Pin missing in inCfg " );
          xmlPiRail.serialPrintlnQName( inCfg->getId() );
        }
      }
      inCfg->attached = inMode;
      return true;
    }
    else {
      return (inCfg->attached == inMode);
    }
  }
}

bool attachPort( PortCfg* portCfg, portMode_t portMode ) {
  ExtCfg *extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_NONE) {
      if (portMode == PORTMODE_MOTORDRIVER) {
        PortPin* motorDirPortPin = portCfg->getPortPinByType( PORTPINTYPE_DIRPIN );
        if (motorDirPortPin == NULL) {
          Serial.println( F("Motor dir pin missing") );
          return false;
        }
        int motorDirPin = motorDirPortPin->getPin();
        PortPin* motorPWMPortPin = portCfg->getPortPinByType( PORTPINTYPE_PWM_PIN );
        if (motorPWMPortPin == NULL) {
          Serial.println( F("Motor PWM pin missing") );
          return false;
        }
        int motorPWMPin = motorPWMPortPin->getPin();
        pinMode( motorDirPin, OUTPUT );
        motorPWMPortPin->pwmChannel = nextPwmChannel++;
        ledcSetup( motorPWMPortPin->pwmChannel, 24000, 10 ); // 24 kHz PWM, 10-bit resolution
        ledcAttachPin( motorPWMPin, motorPWMPortPin->pwmChannel ); // assign a led pins to a channel
        Serial.print(F( "  Init motor #PWM=" ));
        Serial.print( motorPWMPin );
        Serial.print(F( ", #dir=" ));
        Serial.println( motorDirPin );
        ledcWrite( motorPWMPortPin->pwmChannel, 0 );
        int sensorPin = portCfg->getPinByType( PORTPINTYPE_SENSORPIN );
        if ((sensorPin >= 32) && (sensorPin <= 39)) {
          adc1_channel_t channel = getADChannel( sensorPin );
          adc1_config_width( ADC_WIDTH_BIT_12 );
          adc1_config_channel_atten( channel, ADC_ATTEN_DB_11 ); // 0 - 2,6V
          adc_power_on();
        }
        portCfg->attached = portMode;
        return true;
      }
      else if (portMode == PORTMODE_IDREADER) {
        // always using normal serial port
        portCfg->attached = portMode;
        return true;
      }
      else if (portMode == PORTMODE_DCCDECODER) {
        portType_t portType = portCfg->getType();
        if (portType == PORTTYPE_MOTORPORT) {
          Serial.print(F( "  Setting up DCC pin=" ));
          int dirPin = portCfg->getPinByType( PORTPINTYPE_DIRPIN );
          if (dirPin < 0) {
            Serial.println(F( " - ERROR dirPin invalid" ));
            return false;
          }
          int pwmPin = portCfg->getPinByType( PORTPINTYPE_PWM_PIN );
          if (pwmPin < 0) {
            Serial.println(F( " - ERROR pwmPin invalid" ));
            return false;
          }
          Serial.print( pwmPin );
          pinMode( pwmPin, OUTPUT );
          digitalWrite( pwmPin, HIGH );
          portCfg->attached = portMode;

          dps.setup( dirPin, 0xFF, true, ROCO );
          dps.setpower( ON );
          dps.setSpeed128( 3, 0 );
          dpsActive = true;
          Serial.println(F( " - DCC initialized" ));
          return true;
        }
        else if (portType == PORTTYPE_BRIDGE) {
          Serial.print(F( "  Setting up DCC pinHigh=" ));
          int pwmPin1 = -1;
          int pwmPin2 = -1;
          PortPin *portPin = (PortPin *) portCfg->firstChild( piRailFactory->ID_PORTPIN );
          while (portPin != NULL) {
            if (portPin->getType() == PORTPINTYPE_PWM_PIN) {
              if (pwmPin1 == -1) {
                pwmPin1 = portPin->getPin();
              }
              else {
                pwmPin2 = portPin->getPin();
              }
            }
            portPin = (PortPin *) portPin->nextSibling( piRailFactory->ID_PORTPIN );
          }
          if (pwmPin1 < 0) {
            Serial.println(F( " - ERROR pwmPin1 invalid" ));
            return false;
          }
          if (pwmPin2 < 0) {
            Serial.println(F( " - ERROR pwmPin2 invalid" ));
            return false;
          }
          Serial.print( pwmPin1 );
          pinMode( pwmPin1, OUTPUT );
          digitalWrite( pwmPin1, HIGH );
          Serial.print(F( ", pinLow=" ));
          Serial.print( pwmPin2 );
          pinMode( pwmPin2, OUTPUT );
          digitalWrite( pwmPin2, LOW );
          portCfg->attached = portMode;

          dps.setup( pwmPin1, pwmPin2, false, ROCO );
          dps.setpower( ON );
          dps.setSpeed128( 3, 0 );
          dpsActive = true;
          Serial.println(F( " - DCC initialized" ));
          return true;
        }
        else {
          Serial.println(F( "Unsupported motor port type" ));
          return false;
        }
      }
      else {
        Serial.println(F( "Unsupported port mode" ));
        return false;
      }
    }
    else {
      return (portCfg->attached == portMode);
    }
  }
  else {
    PortCfg* extPort = xmlPiRail.getCfg()->getPortCfg( extCfg->getPort() );
    if (extPort->attached == PORTMODE_NONE) {
      portMode_t extPortMode = extCfg->getType();
      if (attachPort( extPort, extPortMode )) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return false;
      }
    }
    else {
      if (portCfg->attached == PORTMODE_NONE) {
        portCfg->attached = portMode;
        return true;
      }
      else {
        return (portCfg->attached == portMode);
      }
    }
  }
}

//--------------------------
void setup() {
  sketchNameAndCompileDate = strrchr( sketchFileNameAndCompileDate, '/' );
  if (sketchNameAndCompileDate) {
    sketchNameAndCompileDate ++;
  }
  else {
    sketchNameAndCompileDate = sketchFileNameAndCompileDate;
  }

  pinMode(INFO_LED, OUTPUT);     // Initialize the INFO_LED pin as an output
  digitalWrite(INFO_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  lastErrorMsg[0] = 0;

  Serial.begin( 115200 ); // 74880 ); // 2400 );
  inputPos = 0;
  Serial.println();
  Serial.println( sketchNameAndCompileDate );
  Serial.println();
  printHeap( F("Begin setup") );

  piRailFactory->max_csv_count = 5;
  piRailFactory->max_ev_count = 4;
  piRailFactory->max_fileDef_count = 10;

  //--- From https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html :
  // Please do not use the interrupt of GPIO36 and GPIO39 when using ADC or Wi-Fi with sleep mode enabled.
  // Please refer to the comments of adc1_get_raw. Please refer to section 3.11 of 
  // ‘ECO_and_Workarounds_for_Bugs_in_ESP32’ for the description of this issue. 
  // As a workaround, call adc_power_acquire() in the app. This will result in higher power consumption
  // (by ~1mA), but will remove the glitches on GPIO36 and GPIO39.
  adc_power_acquire();

  data = new Data();
  xmlPiRail.setup( WiFi.macAddress().c_str(), DEVICETYPE_LOCOMOTIVE );

  // Ensure we have a hostname before connectToWifi()
  Cfg* cfg = xmlPiRail.getCfg();
  const char *deviceName;
  if ((cfg != NULL) && (cfg->getId() != NULL) && (strlen(cfg->getId()->getName()) > 0)) {
    deviceName = cfg->getId()->getName();
    setUpHostname( deviceName, false );
  }
  else {
    deviceName = xmlPiRail.getDeviceID();
    if (deviceName == NULL) {
      deviceName = "INIT";
    }
    setUpHostname( deviceName, true );
  }

  if (!initialConfigMode) {
    initialConfigMode = !connectToWifi();
  }

  if (!initialConfigMode && !xmlPiRail.isErrorMode()) {
    Serial.print( F("Start listen UDP ") );
    Serial.println( piRailReceivePort );
    // if you get a connection, report back via serial:
    Udp.begin( piRailReceivePort );
    xmlPiRail.begin( udpOut, -1 );
  }

  if (initialConfigMode) {
    // discard the hostname and rebuild
    setUpHostname( deviceName, true );
  }

  Serial.print( F("Hostname=") );
  Serial.println( myHostname );

  if (initialConfigMode) {
    setUpConfigWifi();
  }

  delay(250);

  FileCfg* fileCfg = xmlPiRail.getFileCfg();
  NetCfg* netCfg = xmlPiRail.getNetCfg();
  piHttpServer.init( sketchNameAndCompileDate, myHostname, fileCfg, netCfg );
  piHttpServer.begin();

  piOta.begin( myHostname );

  //--- Initialize RFID reader
  nfc.begin();
  rfidPN532 = false;
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (!versiondata) {
    Serial.println("Didn't find PN53x board");
  }
  else {
    rfidPN532 = true;
    Serial.println("Found chip PN5");
    // Set the max number of retry attempts to read from a card
    // This prevents us from waiting forever for a card, which is
    // the default behaviour of the PN532.
    nfc.setPassiveActivationRetries( 1 );
    // configure board to read RFID tags
    nfc.SAMConfig();
  }

  printHeap( F("Finished setup") );
}

//------------------------------------------------------------------
// Hardware access functions
//------------------------------------------------------------------

int readMotorSensor( PortCfg* portCfg, int currentSpeed ) {
  if (portCfg == NULL) {
    return 0;
  }
  int sensorPin = portCfg->getPinByType( PORTPINTYPE_SENSORPIN );
  PortPin* motorPWMPortPin = portCfg->getPortPinByType( PORTPINTYPE_PWM_PIN );
  if (motorPWMPortPin == NULL) {
    return 0;
  }
  int pwmChannel = motorPWMPortPin->pwmChannel;
  if (pwmChannel < 0) {
    return 0;
  }

  if ((sensorPin >= 32) && (sensorPin <= 39)) {
    adc1_channel_t channel = getADChannel( sensorPin );
    ledcWrite( pwmChannel, 1023 );
    delay( 1 );
    int val = 0;
    int count = 8;
    for (int i = 0; i < count; i++) {
      val += adc1_get_raw( channel );
    }
    ledcWrite( pwmChannel, currentSpeed );

    int sensorValue = val / count;
    return sensorValue;
  }
  else {
    return 0;
  }
}

MsgState* getMsgState( const QName* msgID ) {
  for (int i = 0; i < MAX_MSG_STATE; i++) {
    if (msgStateList[i].getId() == msgID) {
      return &msgStateList[i];
    }
  }
  return NULL;
}

void processMsgState( MsgState* newMsgState ) {
  const QName* msgID = newMsgState->getId();
  MsgState* msgState = getMsgState( msgID );
  if (msgState == NULL) {
    msgState = getMsgState( NULL ); // get first unused entry
    if (msgState != NULL) {
      msgState->setId( msgID );
    }
  }
  if (msgState != NULL) {
    msgState->setPos( newMsgState->getPos() );
    msgState->setDist( newMsgState->getDist() );
    msgState->setCmd( newMsgState->getCmd() );
    msgState->setCmdDist( newMsgState->getCmdDist() );
  }
}

const QName* checkIDReader( SensorAction* sensorAction ) {
  if (!rfidPN532) {
    return NULL;;
  }

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  boolean success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength);

  if (success) {
    idReadMillis = millis();
    for (int i = 0; i < uidLength; i++) {
      String tmp = String(uid[i], HEX);
      if (tmp.length() == 1) {
        idReaderData[2*i] = '0';
        idReaderData[2*i+1] = tmp.charAt(0);
      }
      else {
        idReaderData[2*i] = tmp.charAt(0);
        idReaderData[2*i+1] = tmp.charAt(1);
      }
    }
    idReaderData[2*uidLength] = 0;

    const QName* msgID = piRailFactory->NS_PIRAILFACTORY->getQName( idReaderData, 0, 2*uidLength );
    MsgState* msgState = getMsgState( msgID );
    if (msgState == NULL) {
      xmlPiRail.processTrackMsg( sensorAction, msgID, 0, 0, 0, idReadMillis );
      return msgID;
    }
    else {
      const QName* posID = msgState->getPos();
      if (posID == NULL) {
        posID = msgID;
      }
      int dist_mm = msgState->getDist() * 10; //mm
      int cmd = msgState->getCmd();
      int cmdDist_mm = msgState->getCmdDist() * 10; //mm
      xmlPiRail.processTrackMsg( sensorAction, posID, dist_mm, cmd, cmdDist_mm, idReadMillis );
      return posID;
    }
  }
  return NULL;
}

void doMotor( PortCfg* portCfg, char dir, int motorOutput ) {
  if (portCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = portCfg->getExtCfg();
  if (extCfg == NULL) {
    if (portCfg->attached == PORTMODE_MOTORDRIVER) {
      int pinDir = portCfg->getPinByType( PORTPINTYPE_DIRPIN );
      if (pinDir < 0) {
        return;
      }
      PortPin *motorPWMPortPin = portCfg->getPortPinByType( PORTPINTYPE_PWM_PIN );
      if (motorPWMPortPin == NULL) {
        return;
      }
      int pwmChannel = motorPWMPortPin->pwmChannel;
      if (pwmChannel < 0) {
        return;
      }

      //--- Set PWM and Dir
      if (dir == DIR_FORWARD) {
        digitalWrite( pinDir, HIGH );
      }
      else {
        digitalWrite( pinDir, LOW );
      }
      ledcWrite( pwmChannel, motorOutput );
    }
  }
  else {
    if (extCfg->getType() == PORTMODE_DCCDECODER) {
      ExtCfg* extCfg = portCfg->getExtCfg();
      uint16_t lokID = extCfg->getBusAddr();
      uint8_t dccSpeed = (motorOutput >> 3); // reduce 0..1023 to 0..127
      if (dir == DIR_FORWARD) {
        dccSpeed = dccSpeed + 128;
      }
      dps.setSpeed128( lokID, dccSpeed );
      dps.update();
    }
  }
}

void doOutputPin( OutCfg* outCfg, int pinValue, int duration ) {
  if (outCfg == NULL) {
    return;
  }
  ExtCfg* extCfg = outCfg->getExtCfg();
  if (extCfg == NULL) {
    int pin = outCfg->getPin();
    if (pin < 0) {
      return;
    }
    outMode_t outMode = outCfg->attached;
    switch (outMode) {
      case OUTMODE_SERVO: {
        int index = outCfg->index;
        Serial.print(F( "  Set servo" ));
        Serial.print( index );
        Serial.print(F( " pin=" ));
        Serial.print( pin );
        Serial.print(F( "->" ));
        Serial.println( pinValue );
        if (index == 1) {
          servo1.write( pinValue );
        }
        else if (index == 2) {
          servo2.write( pinValue );
        }
        break;
      }
      case OUTMODE_PWM: {
        Serial.print(F( "  Set PWM pin " ));
        Serial.print( pin );
        ledcWrite( outCfg->pwmChannel, pinValue );
        Serial.println( pinValue );
        break;
      }
      case OUTMODE_SWITCH: {
        Serial.print(F( "  Set port " ));
        Serial.print( pin );
        if (pinValue == 0) {
          digitalWrite( pin, LOW );
          Serial.println( " off" );
        }
        else if (pinValue == -1) {
          digitalWrite( pin, HIGH );
          Serial.println( " on" );
        }
        break;
      }
      default: { // PULSE or NONE
        if ((duration <= 0) || (duration > 500)) {
          duration = 500;
        }
        Serial.print(F( "  Pulse pin " ));
        Serial.print( pin );
        Serial.print( F(", val=") );
        Serial.print( pinValue );
        Serial.print( F(", len=") );
        Serial.println( duration );
        if (pinValue < 0) {
          digitalWrite( pin, HIGH );
        }
        else {
          Serial.print( F("doPulse NOT YET IMPLEMENTED for value=") );
          Serial.println( pinValue );
          //analogWrite( swPort, pinValue );
        }
        delay( duration );
        digitalWrite( pin, LOW );
      }
    }
  }
  else if (extCfg->getType() == PORTMODE_DCCDECODER) {
    int fn = outCfg->getPin();
    if (outCfg->attached == OUTMODE_SWITCH) {
      if (pinValue != 0) {
        pinValue = 1;
      }
    }
    else {
      pinValue = 2; // toggle
    }
    uint16_t locoAddr = extCfg->getBusAddr();
    Serial.print( F("Set DCC addr=") );
    Serial.print( locoAddr );
    Serial.print( F(", f") );
    Serial.print( fn );
    Serial.print( F("=") );
    Serial.println( pinValue );
    dps.setLocoFunc( locoAddr, pinValue, fn );
    dps.update();
  }
}

void doSendMsg( PortCfg* portCfg, const char* msg ) {
  // not supported - do nothing
}

//------------------------------------------------------------------
// Command Execution (doXXX, processXXX)
//------------------------------------------------------------------

void sendUdpMessage( const char *msg ) {
  //IPAddress sendIP;
  //WiFi.hostByName(udpSendIP, sendIP);
  //Udp.beginPacketMulticast( WiFi.localIP(), sendIP, piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast( sendIP, piRailSendPort, WiFi.localIP() ); // funzt nicht
  //Udp.beginPacket( "192.168.43.50", piRailSendPort ); // funzt
  //Udp.beginPacket( "255.255.255.255", piRailSendPort ); // funzt nicht
  //Udp.beginPacketMulticast(addr, port, WiFi.localIP())
  IPAddress sendIP = WiFi.localIP();
  sendIP[3] = 255;
  Udp.beginPacket( sendIP, piRailSendPort ); // funzt
  Udp.print( msg );
  Udp.endPacket();
  udpOut[0] = 0; // clear udpOut buffer
}

//------------------------------------------------------------------
// Serial Event Handling
//------------------------------------------------------------------
/*
 SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();

    //----- Add char to ID reader input
    if (inChar == '[') { // start flag
      idReaderPos = 0;
      idReadMillis = millis();
    }
    else if (idReaderPos >= 0) {
      if (inChar == ']') {
        if (idReaderPos == 7) {
          // checksum
          int cksum = 0;
          for (int i = 0; i < idReaderPos-1; i++) {
            cksum += (byte) idReaderData[i];
          }
          cksum = cksum & 0x7F;
          if (cksum < 32) {
            cksum += 32; // avoid using control characters
          }
          if ((cksum == '[') || (cksum==']')) {
            cksum++; // avoid using mesage start/end characters
          }
          char ckSumCh = (char) cksum;

          if (ckSumCh == idReaderData[6]) {
            // replace checksum byte with string end
            idReaderData[6] = 0;
            bool repeated = false;

            //-- Process speed, dir and command
            int dist_mm = (idReaderData[3] - 32) * 20; //mm
            const QName* posID = piRailFactory->NS_PIRAILFACTORY->getQName( idReaderData, 0, 3 );
            char cmd = idReaderData[4];
            int cmdDist_mm = (idReaderData[5] - 32) * 20;
            if (xmlPiRail.processTrackMsg( xmlPiRail.getIDSensorAction(), posID, dist_mm, cmd, cmdDist_mm, idReadMillis )) {
              serialChangedState = true;
            }
            idReaderPos = -1;
            return; // leave method to allow processing of received message
          }
        }
        idReaderPos = -1;
      }
      else {
        if (idReaderPos < MSG_MAX_LEN) {
          idReaderData[idReaderPos] = inChar;
          idReaderPos++;
        }
        else {
          idReaderPos = -1;
        }
      }
    }
  }
}

void debugMsg( const char* msg, const QName* id, int val ) {
  Csv* csv = data->addCsv( CSVTYPE_DEBUGDATA );
  if (csv == NULL) { // buffer is full --> send
    sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
    data->clear();
    csv = data->addCsv( CSVTYPE_DEBUGDATA );
  }
  csv->appendStr( msg );
  if (id == NULL) {
    csv->appendStr( "null" );
    csv->appendStr( "null" );
  }
  else {
    csv->appendStr( id->getNamespace()->getUri() );
    csv->appendStr( id->getName() );
  }
  csv->append( val );
  sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
  data->clear();
}

//==========================
// Main Loop
//==========================
void loop() {
  if ( resetMillisStart != 0L && (unsigned long)( millis() - resetMillisStart ) >= resetAfterMillis ) {
    Serial.println();
    Serial.print( F("Performing reset at ") );
    Serial.println( millis() );
    ESP.restart();
  }

  ArduinoOTA.handle();

  if (initialConfigMode) {
    if (!httpServerWasAccessed && millis() >= 3 * 60 * 1000) {
      // 3 minutes = 1 minute trying to connect to existing WiFi plus 2 minutes configuration window
      Serial.println();
      Serial.println( F("Config timeout. Restarting…") );
      Serial.println();
      ESP.restart();
    }
    return; // return even if it's not yet time to restart
  }

  //checkWiFi();

  if (xmlPiRail.isErrorMode()) {
    unsigned long currentTime = millis();
    long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      IPAddress sendIP = WiFi.localIP();
      sendIP[3] = 255;
      Udp.beginPacket( sendIP, piRailSendPort ); // funzt
      const char* deviceID = xmlPiRail.getDeviceID();
      if ((deviceID != NULL) && (strlen(deviceID) > 0)) {
        Udp.print( deviceID );
      }
      else {
        Udp.print( myHostname );
      }
      Udp.print( ":\n" );
      Udp.print( sketchNameAndCompileDate );
      Udp.print( "\nERROR - " );
      Udp.print( lastErrorMsg );
      Udp.endPacket();
      udpOut[0] = 0; // clear udpOut buffer
      xmlPiRail.setNextSyncTime( currentTime + 5000 );
    }
  }
  else {
    bool sendState = false;
    int packetSize = Udp.parsePacket();
    if (packetSize > 0) {
      IPAddress remoteIp = Udp.remoteIP();
      sendState = xmlPiRail.processXml( WiFi.localIP(), remoteIp, &Udp );
    }
    //--- Event processing and notification
    else if (Serial.available()) {
      // ESP32 seems to miss implementation for calling serialEvent() like ESP8266
      serialEvent();
      if (serialChangedState) {
        sendState = true;
        serialChangedState = false;
      }
    }
    else {
      if (dpsActive) {
        dps.update();
      }
    }

    //--- accelerate or slow down
    Cfg* cfg = xmlPiRail.getCfg();
    Ln* ln = xmlPiRail.getDeviceState()->getLn();
    MotorAction* motorAction = (MotorAction*) cfg->firstChild( piRailFactory->ID_MOTOR );
    while (motorAction != NULL) {
      int oldSpeed = motorAction->getState()->getCurI();
      motorAction->doLoop( data, ln );
      int newSpeed = motorAction->getState()->getCurI();
      if (oldSpeed != newSpeed) {
        sendState = true;
      }
      motorAction = (MotorAction*) motorAction->nextSibling( piRailFactory->ID_MOTOR );
    }

    //---- Check if a input event has occurred
    IoCfg* ioCfg = xmlPiRail.getIoCfg();
    unsigned long evTime = millis();
    portENTER_CRITICAL( &mux );
    if (inputEventOccurred) {
      inputEventOccurred = false;
      SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
      while (sensorAction != NULL) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if ((inCfg != NULL) && (inCfg->eventOccurred)) {
          sensorAction->eventOccurred = true;
          inCfg->eventOccurred = false;
        }
        sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
      }
    }
    portEXIT_CRITICAL( &mux );

    //--- Process input events and polling
    SensorAction* sensorAction = (SensorAction*) cfg->firstChild( piRailFactory->ID_SENSOR );
    while (sensorAction != NULL) {
      if (sensorAction->eventOccurred
          || ((sensorAction->nextPoll > 0) && (sensorAction->nextPoll < evTime))) {
        InCfg* inCfg = sensorAction->getInCfg( cfg );
        if (inCfg != NULL) {
          int pin = inCfg->getPin();
          if (pin > 0) {
            int value = digitalRead( pin );
            char charVal;
            if (value == HIGH) {
              charVal = '1';
            }
            else {
              charVal = '0';
            }
            if (sensorAction->processEvent( NULL, charVal, evTime, cfg, ioCfg )) {
              sendState = true;
            }
          }
        }
      }
      sensorAction = (SensorAction*) sensorAction->nextSibling( piRailFactory->ID_SENSOR );
    }

    //--- Check timers and execute if time out
    TimerAction* timerAction = (TimerAction*) cfg->firstChild( piRailFactory->ID_TIMER );
    while (timerAction != NULL) {
      if (timerAction->doLoop( cfg, ioCfg )) {
        sendState = true;
      }
      timerAction = (TimerAction*) timerAction->nextSibling( piRailFactory->ID_TIMER );
    }

    //--- Send sync message
    unsigned long currentTime = millis();
    unsigned long nextSyncTime = xmlPiRail.getNextSyncTime();
    if ((nextSyncTime >= 0) && (currentTime >= nextSyncTime)) {
      sendState = true;
      xmlPiRail.setNextSyncTime( -1 );
    }
    if (sendState) {
      // print events and speed changes into udpOut
      xmlPiRail.sendDeviceState( currentTime, WiFi.RSSI() );
    }

    //--- Check triggers
    xmlPiRail.processState( WiFi.localIP(), WiFi.localIP(), xmlPiRail.getDeviceState() );

    //--- Send motor tuning data if buffer is full
    if (data->isFull()) {
      sendUdpMessage( xmlPiRail.writeData( data ) ); // prints csv data to udpOut
      data->clear();
    }
  }
}
