/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <Arduino.h>
#include "PiOTA.h"

extern int INFO_LED;
extern int LED_OFF;
extern int LED_ON;
extern int ledSpeedDefault;
extern int ledSpeed;

extern bool toggleLed();

void PiOTA::startOTA() {
  String type;
  int blinkCount = 0;

  //if the update is burning in external flash memory, then show "flash"
  Serial.print( F("OTA Start updating ") );
  if (ArduinoOTA.getCommand() == U_FLASH) {
    Serial.println( F("flash") );
    blinkCount = 3;
  }
  else {  //if the update is on the internal memory (file system),then show "filesystem"
    Serial.println( F("filesystem") );
    blinkCount = 5;
  } // U_SPIFFS

  digitalWrite(INFO_LED, LED_OFF);
  delay(500);
  for (int i = 0; i < blinkCount; i++) {
    digitalWrite(INFO_LED, LED_ON);
    delay(100);
    digitalWrite(INFO_LED, LED_OFF);
    delay(100);
  }
  delay(500);

  ledSpeed = 25;
}

//shows the progress in percent
void PiOTA::progressOTA( unsigned int progress, unsigned int total ) {
  if (toggleLed()) {
    Serial.print(F( "OTA Progress: " ));
    Serial.print(progress / (total / 100));
    Serial.println("%");
  }
}

//just show message "End"
void PiOTA::endOTA() {
  Serial.println();
  Serial.println( F("OTA End") );

  ledSpeed = ledSpeedDefault;

  digitalWrite(INFO_LED, LED_ON);
  delay(1000);
  digitalWrite(INFO_LED, LED_OFF);

  delay(500);
}

//if an error occurred, shows especificaly the error type
void PiOTA::errorOTA( ota_error_t error ) {
  Serial.print( F("OTA Error ") );
  Serial.println( error );

  ledSpeed = ledSpeedDefault;

  int blinkCount = 0;

  if (error == OTA_AUTH_ERROR) {
    Serial.println( F("Auth Failed") );
    blinkCount = 1;
  }
  else if (error == OTA_BEGIN_ERROR) {
    Serial.println( F("Begin Failed") );
    blinkCount = 2;
  }
  else if (error == OTA_CONNECT_ERROR) {
    Serial.println( F("Connect Failed") );
    blinkCount = 3;
  }
  else if (error == OTA_RECEIVE_ERROR) {
    Serial.println( F("Receive Failed") );
    blinkCount = 4;
  }
  else if (error == OTA_END_ERROR) {
    Serial.println( F("End Failed") );
    blinkCount = 5;
  }

  digitalWrite(INFO_LED, LED_OFF);
  delay(500);

  if (blinkCount > 0) {
    for (int i = 0; i < blinkCount; i++) {
      digitalWrite(INFO_LED, LED_ON);
      delay(100);
      digitalWrite(INFO_LED, LED_OFF);
      delay(100);
    }
  }
  else {
    digitalWrite(INFO_LED, LED_ON);
    delay(500);
    digitalWrite(INFO_LED, LED_OFF);
  }

  delay(500);
}

void PiOTA::begin( const char *hostname ) {
  ArduinoOTA.setHostname(hostname);

  //configures what will be executed when ArduinoOTA starts
  ArduinoOTA.onStart(PiOTA::startOTA);

  //configures what will be executed when ArduinoOTA ends
  ArduinoOTA.onEnd(PiOTA::endOTA);

  //configures what will be executed when the sketches is burning in the esp
  ArduinoOTA.onProgress(PiOTA::progressOTA);

  //configures what will be executed when ArduinoOTA finds an error
  ArduinoOTA.onError(PiOTA::errorOTA);

  //initializes ArduinoOTA
  ArduinoOTA.begin();
}
