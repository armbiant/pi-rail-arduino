/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SKETCHBOOK_PIHTTPSERVER_H
#define SKETCHBOOK_PIHTTPSERVER_H

#include <WString.h>
#include "ESPAsyncWebServer.h"
#include "FileCfg.h"
#include "NetCfg.h"

// to enable, change NO_REQUEST_LOGGING to REQUEST_LOGGING
// respectively      NO_UPLOAD_LOGGING to UPLOAD_LOGGING
#define NO_REQUEST_LOGGING
#define NO_UPLOAD_LOGGING

class PiHttpServer {
private:
#ifdef REQUEST_LOGGING
//  static void hexdump(const uint8_t *buffer, size_t packetSize);
  static void dumpHeaders(AsyncWebServerRequest *request);
  static void dumpParameters(AsyncWebServerRequest *request);
#endif

  static void registerFile( const char* path );
  static void printPageTitle(AsyncResponseStream *pStream);
  static int printIndex(AsyncResponseStream *pStream);
  static int printEditWifiForm(AsyncResponseStream *pStream);
  static int printEditForm(AsyncResponseStream *pStream);
  static int printUploadForm(AsyncResponseStream *pStream, const char *path);
  static void onUnhandledRequest(AsyncWebServerRequest *request);

  FileDef* getFileDef( const char* fileName );
public:
  PiHttpServer();
  static void printFileListItem(AsyncResponseStream *pStream, FileDef *fileDef, const char *path);
  void init( const char *sketchNameAndCompileDate, const char *deviceName, FileCfg *fileCfg, NetCfg* netCfg );
  AsyncWebServer *getServer();
  void begin();
};

#endif //SKETCHBOOK_PIHTTPSERVER_H
