/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <Arduino.h>
#include <FS.h>

#include "PiHttpServer.h"

#ifdef ESP32
#include "SPIFFS.h"
#endif

#define PI_HTTP_FILENAME_LENGTH 32
#define PI_HTTP_BUFSIZE 2048

/*

 ToDo: Fehlerbehandlung - momentan wird Quatsch angezeigt, wenn die Datei für download oder edit (noch) nicht da ist.
 ToDo: Nach erfolgreichem Upload redirect zu /index.html, incl. Rückmeldung dass OK
 ToDo: Nach fehlgeschlagenem Upload auf der Seite bleiben, incl. Fehlermeldung
 ToDo: evtl. logs reduzieren

*/

static PROGMEM const char *default_html_1 = "<html>\n"
                                            "  <head>\n"
                                            "    <meta charset=\"UTF-8\">\n"
                                            "    <title>";

static PROGMEM const char *default_html_2 = "</title>\n"
                                            "  </head>\n"
                                            "  <body>\n";

static PROGMEM const char *default_html_Z = "  </body>\n"
                                            "</html>";

static PROGMEM const char *editWifi_html_1 = "    <form action=\"/setwifi\" method=\"post\">\n"
                                             "      <label for=\"ssid\">WLAN SSID:</label><br>\n"
                                             "      <input type=\"text\" id=\"ssid\" name=\"ssid\" value=\"";
static PROGMEM const char *editWifi_html_2 = "\"><br>\n"
                                             "      <label for=\"passwd\">WLAN password:</label><br>"
                                             "      <input type=\"text\" id=\"passwd\" name=\"passwd\" value=\"";
static PROGMEM const char *editWifi_html_3 = "\"><br><br>\n"
                                             "      <input type=\"submit\" value=\"Save\">\n"
                                             "    </form>\n";

static PROGMEM const char *editTextfile_html_1 = "    <form action=\"";

static PROGMEM const char *editTextfile_html_2 = "\" method=\"post\" enctype=\"multipart/form-data\">\n"
                                                 "      <textarea rows=\"20\" cols=\"80\" name=\"filecontent\">";

static PROGMEM const char *editTextfile_html_3 = "</textarea><br>\n"
                                                 "      <button type=\"submit\">Save</button>\n"
                                                 "    </form>\n";

static PROGMEM const char *uploadLink_html = "    <a href=\"/upload.html\">Upload file</a>\n";

static PROGMEM const char *edit_wifi_link_html = "    <a href=\"/editwifi.html\">Edit WiFi SSID and password</a><br>\n";

static PROGMEM const char *uploadForm_html_1 = "    <form action=\"/fileUpload";

static PROGMEM const char *uploadForm_html_2 = "\" method=\"post\" enctype=\"multipart/form-data\">\n"
                                             "      <input type=\"file\" name=\"data\">\n"
                                             "      <p>\n"
                                             "      <strong>Hint:</strong> The uploaded file is always stored with the same file name.\n"
                                             "      <p>\n"
                                             "      <input type=\"submit\" name=\"upload\" value=\"Upload\" title=\"Upload File\">\n"
                                             "    </form>\n";

// Text snippets, not necessarily HTML
static PROGMEM const char *blank = " ";
static PROGMEM const char *slash = "/";
static PROGMEM const char *colon = ":";
static PROGMEM const char *colon_blank = ": ";
static PROGMEM const char *indent_4 = "    ";
static PROGMEM const char *newline  = "\n";

// Single HTML tags
static PROGMEM const char *html_br = "<br>";
static PROGMEM const char *html_p = "<p>";
static PROGMEM const char *html_h3 = "<h3>";
static PROGMEM const char *html_h3_end = "</h3>\n";
static PROGMEM const char *html_h6 = "<h6>";
static PROGMEM const char *html_h6_end = "</h6>\n";
static PROGMEM const char *html_hr = "<hr>";

// HTML Snippets
static PROGMEM const char *fileDelete_html_1 = "<a href=\"/delete.html?file=";
static PROGMEM const char *fileDelete_html_2 = "\">delete</a>";

static PROGMEM const char *fileDownload_html_1 = "<a href=\"";
static PROGMEM const char *fileDownload_html_2 = "\">download</a>";

static PROGMEM const char *fileEdit_html_1 = "<a href=\"/edit.html?file=";
static PROGMEM const char *fileEdit_html_2 = "\">edit</a>";

static PROGMEM const char *picture_html_1 = "<img src=\"";
static PROGMEM const char *picture_html_2 = "\">";

// labels
static const char *label_filelist PROGMEM = ": File list";

// Filename suffixes
static PROGMEM const char *suffix_txt = ".txt";
static PROGMEM const char *suffix_xml = ".xml";
static PROGMEM const char *suffix_jpg = ".jpg";
static PROGMEM const char *suffix_png = ".png";

// mime types
static PROGMEM const char *mimeType_txt = "text/plain";
static PROGMEM const char *mimeType_html = "text/html";
static PROGMEM const char *mimeType_xml = "text/xml";
static PROGMEM const char *mimeType_jpg = "image/jpg";
static PROGMEM const char *mimeType_png = "image/png";
static PROGMEM const char *mimeType_other = "application/octet-stream";

// messages
static PROGMEM const char *httpStatusMessage_OK = "OK";
static PROGMEM const char *httpStatusMessage_FILE_NOT_FOUND = "File not found.";
static PROGMEM const char *httpStatusMessage_FS_ERROR = "File System Error.";
static PROGMEM const char *httpStatusMessage_SSID_EMPTY = "SSID must not be empty";

static PROGMEM const char *httpStatusMessage_FILE_NOT_DELETABLE = "File is configured as not deletable.";

static PROGMEM const char *httpStatusMessage_MISSING_PARAMETER_FILE = "Missing parameter: \"file\".";

//page names
static PROGMEM const char* INDEX_HTML = "/index.html";
static PROGMEM const char* EDITWIFI_HTML = "/editwifi.html";
static PROGMEM const char* EDIT_HTML = "/edit.html";
static PROGMEM const char* DELETE_HTML = "/delete.html";
static PROGMEM const char* UPLOAD_HTML = "/upload.html";
static PROGMEM const char* SETWIFI = "/setwifi";
static PROGMEM const char* FILE_UPLOAD = "/fileUpload";

// other constants
static PROGMEM const char *icon_files[] = { "/icon.jpg" };

static PROGMEM const char* PARAM_SSID = "ssid";
static PROGMEM const char* PARAM_PASSWD = "passwd";
static PROGMEM const char* PARAM_PATH = "path";
static PROGMEM const char* FILECONTENT = "filecontent";

const char *piSketchNameAndCompileDate;
const char *piDeviceName;
static FileCfg *piFileCfg;
static NetCfg *piNetCfg;
static XmlWriter* _xmlWriter;

char filenameToEdit[PI_HTTP_FILENAME_LENGTH + 1];
uint8_t buffer[PI_HTTP_BUFSIZE + 1];

extern void doReset( long afterMillis );
extern void httpServerAccessed();

AsyncWebServer server(80);

PiHttpServer::PiHttpServer() {
}

void checkFilenameAndReboot(const char *path) {
  const char *fg_xml = strstr( path, "fg.xml" );
  if (fg_xml && fg_xml > path) {
    fg_xml --;
    if (*fg_xml == 'c' || *fg_xml == 'C') {
      doReset( 2500 );
    }
  }
}

void handleSetWifi( AsyncWebServerRequest *request ) {
  static String      path = "/netCfg.xml";
  static String      ssid;
  static String      passwd;
  static File        file;                              // File handle output file
  static int         httpStatus = 200;
  static const char* message = httpStatusMessage_OK;

  httpServerAccessed();
  Serial.println();
  Serial.println(F( "SetWifiStart" ));

  ssid = request->arg( PARAM_SSID );
  passwd = request->arg( PARAM_PASSWD );
  if (ssid.isEmpty()) {
    httpStatus = 500;
    message = httpStatusMessage_SSID_EMPTY;
    request->send( httpStatus, mimeType_txt, message );
  }
  else {
    WifiCfg* wifiCfg = piNetCfg->getWifiCfg();
    if (wifiCfg == NULL) {
      wifiCfg = (WifiCfg *) piRailFactory->getNewWifiCfg( piNetCfg, piRailFactory->ID_WIFICFG );
      piNetCfg->addChild( piRailFactory->ID_WIFICFG, wifiCfg, NULL );
    }
    wifiCfg->setSsid( (char*) ssid.c_str() );
    wifiCfg->setPass( (char*) passwd.c_str() );

    if (SPIFFS.exists( path )) {
      SPIFFS.remove( path );                             // Remove old file
#ifdef UPLOAD_LOGGING
      Serial.print( F("removed old file ") );
      Serial.println(path);
#endif
    }
    file = SPIFFS.open( path, "w" );                     // Create new file
    if (file) {                                       // file write error
#ifdef UPLOAD_LOGGING
      Serial.print( F("opened file ") );
      Serial.print(path);
      Serial.println( F(" for writing") );
#endif
      _xmlWriter->begin( piRailFactory->NS_PIRAILFACTORY );
      piNetCfg->writeXml( piRailFactory->ID_NETCFG, _xmlWriter );
      const char* xmldata = _xmlWriter->getXml();
      int xmllen = strlen( xmldata );

      if ( file.write( (uint8_t *) xmldata, xmllen ) != xmllen ) {        // file write error
        httpStatus = 500;
        message = httpStatusMessage_FS_ERROR;
      }
      file.flush();
      file.close();
#ifdef UPLOAD_LOGGING
        Serial.print( F("closed file ") );
        Serial.println(path);
#endif
      Serial.println();
      Serial.print( F("SetWifiEnd: updated netCfg.xml") );
      Serial.print(", ");
      Serial.print(xmllen);
      Serial.println("B");
      request->send( httpStatus, mimeType_txt, message );

      checkFilenameAndReboot(path.c_str());
    }
    else {
#ifdef UPLOAD_LOGGING
      Serial.print( F("Error opening file ") );
      Serial.print(path);
      Serial.println( F(" for writing") );
#endif
      httpStatus = 500;
      message = httpStatusMessage_FS_ERROR;
      request->send( httpStatus, mimeType_txt, message );
    }
  }
}

void handleFileUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) {
  static String      path;
  static File        file;                              // File handle output file
  static uint32_t    totallength;                       // Total file length
  static size_t      lastindex;                         // To test same index
  static int         httpStatus = 200;
  static const char* message = httpStatusMessage_OK;

  if ( index == 0 ) {
    httpServerAccessed();

    Serial.println();
    Serial.print( F("UploadStart: ") );
    Serial.println(filename);

    // Form SPIFFS filename
    path.clear();
    AsyncWebParameter *parameter = request->getParam(PARAM_PATH);
    if (parameter != NULL) {
      if (!parameter->value().startsWith(slash)) {
        path += slash;
      }
      path += parameter->value();
    }
    if (!path.endsWith(slash) && !filename.startsWith(slash)) {
      path += slash;
    }
    path += filename;

    if (SPIFFS.exists(path)) {
      SPIFFS.remove( path );                             // Remove old file
#ifdef UPLOAD_LOGGING
      Serial.print( F("removed old file ") );
      Serial.println(path);
#endif
    }
    file = SPIFFS.open( path, "w" );                     // Create new file
    if ( !file ) {                                       // file write error
#ifdef UPLOAD_LOGGING
      Serial.print( F("Error opening file ") );
      Serial.print(path);
      Serial.println( F(" for writing") );
#endif
      httpStatus = 500;
      message = httpStatusMessage_FS_ERROR;
    }
#ifdef UPLOAD_LOGGING
    else {
      Serial.print( F("opened file ") );
      Serial.print(path);
      Serial.println( F(" for writing") );
    }
#endif
    totallength = 0;                                 // Total file lengt still zero
    lastindex = 0;                                   // Prepare test
  }

  if ( len )                                         // Something to write?
  {
    if ( ( index != lastindex ) || ( index == 0 ) )  // New chunk?
    {
      if ( httpStatus == 200 ) {                     // No error so far?
#ifdef UPLOAD_LOGGING
        Serial.write(data, len);
#endif

        if ( file.write( data, len ) != len ) {        // file write error
          httpStatus = 500;
          message = httpStatusMessage_FS_ERROR;
        }
      }
      totallength += len;
      lastindex = index;
    }
  }

  if ( final ) {
    file.close();
#ifdef UPLOAD_LOGGING
    Serial.print( F("closed file ") );
    Serial.println(filename);
#endif

    Serial.println();
    Serial.print( F("UploadEnd: ") );
    Serial.print(filename);
    Serial.print(", ");
    Serial.print(totallength);
    Serial.println("B");
    request->send( httpStatus, mimeType_txt, message );

    checkFilenameAndReboot(path.c_str());
  }
}

#ifdef REQUEST_LOGGING
void PiHttpServer::dumpHeaders(AsyncWebServerRequest *request) {
  Serial.println();
  Serial.print(request->methodToString());
  Serial.print(" ");
  Serial.println(request->url());

  int headers = request->headers();
  int i;

  //List all collected headers
  for(i=0;i<headers;i++){
    AsyncWebHeader* h = request->getHeader(i);
    Serial.print( F("HEADER[") );
    Serial.print(h->name());
    Serial.print( F("]: ") );
    Serial.println(h->value());
  }

/*
  //get specific header by name
  if(request->hasHeader("MyHeader")){
    AsyncWebHeader* h = request->getHeader("MyHeader");
    Serial.print("MyHeader: ");
    Serial.println(h->value());
  }
*/
}

void PiHttpServer::dumpParameters(AsyncWebServerRequest *request) {
  int params = request->params();

  if (params == 0) {
    Serial.println( F("No params.") );
  }
  for(int i=0;i<params;i++){
    AsyncWebParameter* p = request->getParam(i);
    if(p->isFile()){ //p->isPost() is also true
      Serial.print( F("FILE[") );
      Serial.print(p->name());
      Serial.print( F("]: ") );
      Serial.print(p->value());
      Serial.print( F(", size: ") );
      Serial.println(p->size());
    } else if(p->isPost()){
      Serial.print( F("POST[") );
      Serial.print(p->name());
      Serial.print( F("]: ") );
      Serial.println(p->value());
    } else {
      Serial.print( F("GET[") );
      Serial.print(p->name());
      Serial.print( F("]: ") );
      Serial.println(p->value());
    }
  }
}
#endif

void PiHttpServer::registerFile( const char *path ) {
  Serial.print( F("RegisterFile ") );
  Serial.println( path );
  server.on(path, HTTP_POST,
            [path](AsyncWebServerRequest *request) {
              httpServerAccessed();
#ifdef REQUEST_LOGGING
              dumpHeaders(request);
              dumpParameters(request);
#endif
              if (request->hasParam( FILECONTENT, true, false)) {
                AsyncWebParameter *parameter = request->getParam( FILECONTENT, true, false);
                if ( parameter != NULL ) {
#ifdef UPLOAD_LOGGING
                  Serial.println( F("File content :") );
                  Serial.println(parameter->value().c_str());
#endif
                  File file = SPIFFS.open(path, "w");
                  if( file ) {
                    file.print(parameter->value().c_str());
                    file.close();
                  } else {
                    Serial.print( F("There was an error opening the file \"") );
                    Serial.print(path);
                    Serial.println( F("\" for writing") );
                    return;
                  }
                } else {
                  Serial.println( F("parameter \"filecontent\" is here but null") );
                }
              }
              request->send_P(200, mimeType_txt, "OK.");
              checkFilenameAndReboot(path);
            }
  );
}

void PiHttpServer::printPageTitle(AsyncResponseStream *pStream) {
  pStream->print(indent_4);
  pStream->print(html_h3);
  for (const char *iconFile : icon_files) {
    if (SPIFFS.exists(iconFile)) {
      pStream->print(picture_html_1);
      pStream->print(iconFile);
      pStream->print(picture_html_2);
      pStream->print(html_br);
      break;
    }
  }
  pStream->print(piDeviceName);
}

void PiHttpServer::printFileListItem(AsyncResponseStream *pStream, FileDef *fileDef, const char *path) {

  // skip icon in file list, because it's already shown beside header
  bool isIcon = false;
  for (const char *iconFile : icon_files) {
    if (!strcmp(iconFile, path)) {
      isIcon = true;
      break;
    }
  }

  bool skip = isIcon;

  /*
     fileDef is NULL when we have been called from the "list files in SPIFFS section" (additional file).
     in this case, skip all files known by configuration, because they already have been listed.
  */
  if (!skip && !fileDef) {
    FileDef *fileDef = (FileDef*) piFileCfg->childAt( piRailFactory->ID_FILEDEF, 0 );
    while (fileDef != NULL) {
      const char *path2 = fileDef->getName();
      if (!strcmp(path, path2)) {
        skip = true;
        break;
      }
      fileDef = (FileDef*) fileDef->nextSibling( piRailFactory->ID_FILEDEF );
    }
  }

  if (!skip) {
    const char *suffix = strrchr( path, '.' );
    if ( suffix && ( !strcmp(suffix, suffix_jpg) || !strcmp(suffix, suffix_png) ) ) {
      if (SPIFFS.exists(path)) {
        pStream->print(indent_4);
        pStream->print(html_br);
        pStream->print(path);
        // additinoal files are always deletable.
        if (!fileDef || fileDef->getDeletable()) {
          pStream->print(blank);
          pStream->print(fileDelete_html_1);
          pStream->print(path);
          pStream->print(fileDelete_html_2);
        }
        pStream->print(html_br);
        pStream->print(picture_html_1);
        pStream->print(path);
        pStream->print(picture_html_2);
        pStream->print(html_br);
        pStream->print(newline);
      }
    } else {
      pStream->print(indent_4);
      pStream->print(path);
      pStream->print(colon_blank);
      pStream->print(fileDownload_html_1);
      pStream->print(path);
      pStream->print(fileDownload_html_2);
      pStream->print(blank);
      pStream->print(fileEdit_html_1);
      pStream->print(path);
      pStream->print(fileEdit_html_2);
      // additinoal files are always deletable.
      if (!fileDef || fileDef->getDeletable()) {
        pStream->print(blank);
        pStream->print(fileDelete_html_1);
        pStream->print(path);
        pStream->print(fileDelete_html_2);
      }
      pStream->print(html_br);
      pStream->print(newline);
    }
  }
}

int PiHttpServer::printIndex(AsyncResponseStream *pStream) {
  int httpStatusCode = 200;

  pStream->print(default_html_1);
  pStream->print(piDeviceName);
  pStream->print(label_filelist);
  pStream->print(default_html_2);

  printPageTitle(pStream);
  pStream->print(label_filelist);
  pStream->print(html_h3_end);

  pStream->print(indent_4);
  pStream->print(html_h6);
  pStream->print(piSketchNameAndCompileDate);
  pStream->print(html_h6_end);
  pStream->print(edit_wifi_link_html);
  pStream->print(indent_4);
  pStream->print(html_p);
  pStream->print(newline);

  pStream->print(indent_4);
  pStream->print(html_hr);
  pStream->print(newline);

  FileDef *fileDef = (FileDef*) piFileCfg->childAt( piRailFactory->ID_FILEDEF, 0 );
  while (fileDef != NULL) {
    const char *path = fileDef->getName();
    printFileListItem(pStream, fileDef, path);
    fileDef = (FileDef*) fileDef->nextSibling( piRailFactory->ID_FILEDEF );
  }

  if (SPIFFS.begin()) {
    pStream->print(indent_4);
    pStream->print(html_hr);
    pStream->print(newline);

#ifdef ESP32
  File root = SPIFFS.open("/");
  File file = root.openNextFile();
  while(file) {
    const char *filename = file.name();
    printFileListItem(pStream, NULL, filename);
    file = root.openNextFile();
  }
#else
    Dir dir = SPIFFS.openDir("/");

    while (dir.next()) {
      String filename = dir.fileName();
      printFileListItem( pStream, NULL, filename.c_str() );
    }
#endif
  }
  pStream->print(uploadLink_html);

  pStream->print(default_html_Z);
  return httpStatusCode;
}

int PiHttpServer::printEditWifiForm(AsyncResponseStream *pStream) {
  int httpStatusCode = 200;

  pStream->print( default_html_1 );
  pStream->print( piDeviceName );
  pStream->print( F(": Edit WiFi") );
  pStream->print( default_html_2 );

  printPageTitle( pStream );
  pStream->print( F(" - Edit WiFi ") );
  pStream->print( html_h3_end );

  WifiCfg* wifiCfg = piNetCfg->getWifiCfg();
  if (wifiCfg == NULL) {
    wifiCfg = (WifiCfg *) piRailFactory->getNewWifiCfg( piNetCfg, piRailFactory->ID_WIFICFG );
    piNetCfg->addChild( piRailFactory->ID_WIFICFG, wifiCfg, NULL );
  }
  pStream->print( editWifi_html_1 );
  const char* ssid = wifiCfg->getSsid();
  if (ssid != NULL) {
    pStream->print( ssid );
  }
  pStream->print( editWifi_html_2 );
  const char* passwd = wifiCfg->getPass();
  if (passwd != NULL) {
    pStream->print( passwd );
  }
  pStream->print( editWifi_html_3 );

  pStream->print( default_html_Z );
  return httpStatusCode;
}

int PiHttpServer::printEditForm(AsyncResponseStream *pStream) {
  int httpStatusCode = 200;

  pStream->print(default_html_1);
  pStream->print(piDeviceName);
  pStream->print( F(": Edit file") );
  pStream->print(default_html_2);

  printPageTitle(pStream);
  pStream->print( F(" - Edit file ") );
  pStream->print(filenameToEdit);
  pStream->print(html_h3_end);

  pStream->print(editTextfile_html_1);
  pStream->print(filenameToEdit);
  pStream->print(editTextfile_html_2);

  if (SPIFFS.exists( filenameToEdit )) {
    File file = SPIFFS.open( filenameToEdit, "r" );
    if (file) {
      size_t available = file.available();
      while (available > 0) {
        memset( buffer, 0, PI_HTTP_BUFSIZE + 1 );
        file.read( buffer, available < PI_HTTP_BUFSIZE ? available : PI_HTTP_BUFSIZE );
        pStream->print((char *) buffer );
        available = file.available();
      }
      file.close();
    }
  }
  pStream->print(editTextfile_html_3);

  pStream->print(default_html_Z);
  return httpStatusCode;
}

int PiHttpServer::printUploadForm(AsyncResponseStream *pStream, const char *path) {
  int httpStatusCode = 200;

  pStream->print(default_html_1);
  pStream->print(piDeviceName);
  pStream->print( F(": Upload file") );
  pStream->print(default_html_2);

  printPageTitle(pStream);
  pStream->print( F(" - Upload file ") );
  pStream->print(html_h3_end);

  pStream->print(uploadForm_html_1);
  if (path != NULL && strlen(path) > 0) {
    pStream->print( F("?path=") );
    pStream->print(path);
  }
  pStream->print(uploadForm_html_2);

  pStream->print(default_html_Z);
  return httpStatusCode;
}

void PiHttpServer::init( const char *sketchNameAndCompileDate, const char *deviceName, FileCfg *fileCfg, NetCfg* netCfg ) {
  piSketchNameAndCompileDate = sketchNameAndCompileDate;
  piDeviceName = deviceName;
  if (fileCfg == NULL) {
    fileCfg = new FileCfg();
  }
  piFileCfg = fileCfg;
  if (netCfg == NULL) {
    netCfg = new NetCfg();
  }
  piNetCfg = netCfg;
  _xmlWriter = new XmlWriter( 300 );
}

AsyncWebServer *PiHttpServer::getServer() {
  return &server;
}

FileDef* PiHttpServer::getFileDef( const char* fileName ) {
  FileDef *fileDef = (FileDef*) piFileCfg->firstChild( piRailFactory->ID_FILEDEF );
  while (fileDef != NULL) {
    const char *path = fileDef->getName();
    if (strcmp(fileName, path)) {
      return fileDef;
    }
    fileDef = (FileDef*) fileDef->nextSibling( piRailFactory->ID_FILEDEF );
  }
  return NULL;
}

void PiHttpServer::begin() {
  server.on( "/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->redirect( "/index.html" );
  });

  server.on( INDEX_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif

    AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
    int httpStatusCode = printIndex( pStream);
    pStream->setCode( httpStatusCode);
    request->send(pStream);
  });

  FileDef *fileDef = (FileDef*) piFileCfg->childAt( piRailFactory->ID_FILEDEF, 0 );
  while (fileDef != NULL) {
    const char *path = fileDef->getName();
    registerFile( path );
    fileDef = (FileDef*) fileDef->nextSibling( piRailFactory->ID_FILEDEF );
  }

  if (SPIFFS.begin()) {
#ifdef ESP32
    File root = SPIFFS.open("/");
    File file = root.openNextFile();
    while(file) {
      const char *path = file.name();
      FileDef* fileDef = getFileDef( path );
      if (fileDef == NULL) {
        int len = strlen(path);
        char* filepath = new char[len+1];
        strcpy( filepath, path );
        filepath[len] = 0;
        registerFile( filepath );
      }
      file = root.openNextFile();
    }
#else
    Dir dir = SPIFFS.openDir( "/" );
    while (dir.next()) {
      String filename = dir.fileName();
      const char *path = filename.c_str();
      FileDef *fileDef = getFileDef( path );
      if (fileDef == NULL) {
        int len = strlen(path);
        char* filepath = new char[len+1];
        strcpy( filepath, path );
        filepath[len] = 0;
        registerFile( filepath );
      }
    }
#endif
  }

  server.on( EDITWIFI_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif
    const char *path = "/netCfg.xml";
    if (SPIFFS.exists(path)) {
      strncpy( filenameToEdit, path, PI_HTTP_FILENAME_LENGTH);
      filenameToEdit[PI_HTTP_FILENAME_LENGTH] = 0;

      AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
      int httpStatusCode = printEditWifiForm(pStream);
      pStream->setCode(httpStatusCode);
      request->send(pStream);
    } else {
      request->send_P(404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND);
    }
  });

  server.on( EDIT_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif
    AsyncWebParameter *parameter = request->getParam("file");
    if ( parameter != NULL ) {
      const char *path = parameter->value().c_str();
      if (SPIFFS.exists(path)) {
        strncpy( filenameToEdit, path, PI_HTTP_FILENAME_LENGTH);
        filenameToEdit[PI_HTTP_FILENAME_LENGTH] = 0;

        AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
        int httpStatusCode = printEditForm(pStream);
        pStream->setCode(httpStatusCode);
        request->send(pStream);
      } else {
        request->send_P(404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND);
      }
    } else {
      request->send_P(400, mimeType_txt, httpStatusMessage_MISSING_PARAMETER_FILE);
    }
  });

  server.on( DELETE_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif
    AsyncWebParameter *parameter = request->getParam("file");
    if ( parameter != NULL ) {
      const char *pathToDelete = parameter->value().c_str();

      if (SPIFFS.exists(pathToDelete)) {
        FileDef *fileDef = (FileDef*) piFileCfg->childAt( piRailFactory->ID_FILEDEF, 0 );
        while (fileDef != NULL) {
          const char *path = fileDef->getName();
          if (!strcmp(path, pathToDelete) && !fileDef->getDeletable()) {
            request->send_P(403, mimeType_txt, httpStatusMessage_FILE_NOT_DELETABLE);
            return;
          }
          fileDef = (FileDef*) fileDef->nextSibling( piRailFactory->ID_FILEDEF );
        }

        if (SPIFFS.remove( pathToDelete )) {
          request->send(200, mimeType_txt, httpStatusMessage_OK);
        } else {
          request->send(500, mimeType_txt, httpStatusMessage_FS_ERROR);
        }
      } else {
        request->send_P(404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND);
      }

    } else {
      request->send_P(400, mimeType_txt, httpStatusMessage_MISSING_PARAMETER_FILE);
    }
  });

  server.on( UPLOAD_HTML, HTTP_GET, [](AsyncWebServerRequest *request) {
    httpServerAccessed();
#ifdef REQUEST_LOGGING
    dumpHeaders(request);
    dumpParameters(request);
#endif

    AsyncWebParameter *parameter = request->getParam(PARAM_PATH);
    AsyncResponseStream *pStream = request->beginResponseStream(mimeType_html);
    int httpStatusCode = printUploadForm(pStream, parameter != NULL ? parameter->value().c_str() : NULL);
    pStream->setCode(httpStatusCode);
    request->send(pStream);
  });

  server.on( SETWIFI, HTTP_POST, [](AsyncWebServerRequest *request) {
    handleSetWifi( request );
  } );

  server.on( FILE_UPLOAD, HTTP_POST, [](AsyncWebServerRequest *request) {
  }, handleFileUpload);

  server.onNotFound(onUnhandledRequest);

  delay(100);

  server.begin();
}

void PiHttpServer::onUnhandledRequest(AsyncWebServerRequest *request) {
  httpServerAccessed();
#ifdef REQUEST_LOGGING
  dumpHeaders(request);
  dumpParameters(request);
#endif
  const String &path = request->url();

  if (!strcmp("GET", request->methodToString()) && SPIFFS.exists( path )) {
    const char *contentType = mimeType_other;
    const char *suffix = strrchr( path.c_str(), '.' );
    if (suffix) {
      if ( !strcmp( suffix, suffix_txt ) ) {
        contentType = mimeType_txt;
      }
      if ( !strcmp( suffix, suffix_xml ) ) {
        contentType = mimeType_xml;
      }
      if ( !strcmp( suffix, suffix_jpg ) ) {
        contentType = mimeType_jpg;
      }
      if ( !strcmp( suffix, suffix_png ) ) {
        contentType = mimeType_png;
      }
    }
#ifdef REQUEST_LOGGING
    Serial.print( F("Sending SPIFFS file ") );
    Serial.println(path);
#endif
    request->send(SPIFFS, path, contentType);
  } else {
    request->send_P(404, mimeType_txt, httpStatusMessage_FILE_NOT_FOUND);
  }
}
