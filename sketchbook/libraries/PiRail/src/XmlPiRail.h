/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PIRAIL_XMLPIRAIL_H
#define PIRAIL_XMLPIRAIL_H

#include "Arduino.h"
#include "PiRailSketch.h"
#include "../../PiMobile/src/Namespace.h"
#include "../../PiMobile/src/XmlReader.h"
#include "../../PiMobile/src/XmlWriter.h"
#include "EnumAction.h"
#include "MotorAction.h"
#include "MsgState.h"
#include "OutCfg.h"
#include "StateScript.h"
#include "WifiCfg.h"
#include "WifiCfgOld.h"
#include "TimerAction.h"
#include "NetCfg.h"
#include "NetCfgOld.h"
#include "FileCfg.h"
#include "IoCfg.h"
#include "Cfg.h"
#include "Cmd.h"
#include "State.h"
#include "Csv.h"
#include "Data.h"

#define LOKO_MAX_SCHEDULE 50

//----- Constants for UPD communication
#define piRailReceivePort 9751
#define piRailSendPort 9750
//static const char udpSendIP[] = "255.255.255.255";

//----- Constants for RFID
#define EVENT_ID_LEN 20
#define EVENT_CMD_LEN 6
#define EVENT_MAX_COUNT 4

#define VAR_MAX_COUNT 16

//-------- XML types union ------------

class XmlPiRail {
private:
  bool errorMode = false;
  const char* _wifiModuleType;
  XmlReader * _xmlReader;
  XmlWriter * _xmlWriter;
  int _receivePos;

  NetCfg* netCfg = NULL;
  NetCfgOld* netCfgOld = NULL;
  FileCfg* fileCfg = NULL;
  IoCfg* ioCfg = NULL;
  Cfg* cfg = NULL;
  State* deviceState = NULL;
  int stateMsgNum = 0;
  int configCrossSum = 0;

  long timeOffset = 0;
  unsigned long nextSyncTime = -1;

  MsgState scheduleList[LOKO_MAX_SCHEDULE];

  Cmd udpCmd;
  State udpState;
  char* _udpOut;
  int _nRTS;
  int xmlBufferSize = 0;
  char* _xmlBuffer;

  Model* loadConfigToXmlBuffer( const char * path );
  int loadConfigFiles();

  void calculateHashes();

  void initPins();
  void initActions();
//  void clearOutdatedEvents( QName* keepId, long millis );

public:
  XmlPiRail( const char* wifiModuleType, int xmlReadBufferSize, int xmlWriteBufferSize );
  void begin( char* udpOut, int nRTS );

  void setup( const char* macAddress, deviceType_t deviceType );

  bool isErrorMode();
  void setErrorMode( bool errorMode );

  void resetXmlBuffer();
  char* getXmlBuffer();

  int getXmlBufferSize();

  const char* getDeviceID();
  const char* getSsid();
  const char* getPass();

  NetCfg *getNetCfg();
  FileCfg *getFileCfg();
  IoCfg *getIoCfg();
  Cfg *getCfg();
  int getCfgCrossSum();

  long getNextSyncTime();
  void setNextSyncTime( long nextSyncTime );
  unsigned int getLockIP( unsigned int ipAddr );

  State* getDeviceState();

  void received( char val );
  Model* parse( Stream* inputStream );

  MsgState* getSchedule4Pos( const QName* posID );
  void processSchedule( MsgState* schedule, int index );
  void processState( unsigned int myIP, unsigned int fromIP, State* deviceState );
  bool processCmd( unsigned int myIP, unsigned int fromIP, Cmd* cmd );
  bool processXml( unsigned int myIP, unsigned int fromIP, Stream* inputStream );

  void sendDeviceState( unsigned long currentTimeMillis, int wifiDB );
  const char* writeData( Data* data );

  SensorAction* getIDSensorAction();
  bool processTrackMsg( SensorAction* sensorAction, const QName* posID, int dist, char cmd, int cmdDist, unsigned long idReadMillis );

//------------------------------------------------------------------
// Serial logging
//------------------------------------------------------------------

  void serialPrintQName( const QName* qName ) const;
  void serialPrintlnQName( const QName* qName ) const;
};


#endif //PIRAIL_XMLPIRAIL_H
