#include "PiRailFactory.h"
#include "Cfg.h"
#include "Cmd.h"
#include "Csv.h"
#include "Data.h"
#include "ExtCfg.h"
#include "FileCfg.h"
#include "FileDef.h"
#include "InCfg.h"
#include "IoCfg.h"
#include "LockCmd.h"
#include "MoCmd.h"
#include "MotorAction.h"
#include "MotorMode.h"
#include "MotorState.h"
#include "NetCfg.h"
#include "OutCfg.h"
#include "Param.h"
#include "PortCfg.h"
#include "PortPin.h"
#include "SecurityCfg.h"
#include "State.h"
#include "SyncCmd.h"
#include "TrackMsg.h"
#include "WifiCfg.h"

PiRailFactory instancePiRailFactory;

PiRailFactory* PiRailFactory::getInstance() {
 return &(instancePiRailFactory);
}

int PiRailFactory::getVersion() {
 return 4;
}
//----------------------------------------------------------
// Manual extensions

extern void printHeap( const __FlashStringHelper* msg );

ActionState* PiRailFactory::getNewActionState( Model* forParent, const QName* forRelation ) {
  ActionState* result = (ActionState*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new ActionState") );
    result = new ActionState();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

CallCmd* PiRailFactory::getNewCallCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new CallCmd") );
  return new CallCmd();
}

Cfg* PiRailFactory::getNewCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Cfg") );
  return new Cfg();
}

Cmd* PiRailFactory::getNewCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Cmd") );
  return new Cmd();
}

Csv* PiRailFactory::getNewCsv( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Csv") );
  return new Csv();
}

Data* PiRailFactory::getNewData( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Data") );
  return new Data();
}

DelayCmd* PiRailFactory::getNewDelayCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new DelayCmd") );
  return new DelayCmd();
}

EnumAction* PiRailFactory::getNewEnumAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new EnumAction") );
  return new EnumAction();
}

ExtCfg* PiRailFactory::getNewExtCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new ExtCfg") );
  return new ExtCfg();
}

FileCfg* PiRailFactory::getNewFileCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new FileCfg") );
  return new FileCfg();
}

FileDef* PiRailFactory::getNewFileDef( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new FileDef") );
  return new FileDef();
}

IfCmd* PiRailFactory::getNewIfCmd( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new IfCmd") );
  return new IfCmd();
}

InCfg* PiRailFactory::getNewInCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new InCfg") );
  return new InCfg();
}

InPin* PiRailFactory::getNewInPin( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new InPin") );
  return new InPin();
}

IoCfg* PiRailFactory::getNewIoCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new IoCfg") );
  return new IoCfg();
}

ItemConn* PiRailFactory::getNewItemConn( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new ItemConn") );
  return new ItemConn();
}

Ln* PiRailFactory::getNewLn( Model* forParent, const QName* forRelation ) {
  Ln* result = (Ln*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new Ln") );
    result = new Ln();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

LockCmd* PiRailFactory::getNewLockCmd( Model* forParent, const QName* forRelation ) {
  Cmd* cmd = (Cmd*) forParent;
  LockCmd* result = (LockCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new LockCmd") );
    result = new LockCmd();
  }
  else {
    cmd->removeChild( result );
  }
  return result;
}

MotorAction* PiRailFactory::getNewMotorAction( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new MotorAction") );
  return new MotorAction();
}

MoCmd* PiRailFactory::getNewMoCmd( Model* forParent, const QName* forRelation ) {
  MoCmd* result = (MoCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new MoCmd") );
    result = new MoCmd();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

MotorMode* PiRailFactory::getNewMotorMode( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new MotorMode") );
  return new MotorMode();
}

MotorState* PiRailFactory::getNewMotorState( Model* forParent, const QName* forRelation ) {
  MotorState* result = (MotorState*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new MotorState") );
    result = new MotorState();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

MsgState* PiRailFactory::getNewMsgState( Model* forParent, const QName* forRelation ) {
  MsgState* result = (MsgState*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new MsgState") );
    return new MsgState();
  }
  else {
    forParent->removeChild( result );
  }
  return result;
}

NetCfg* PiRailFactory::getNewNetCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new NetCfg") );
  return new NetCfg();
}

OutCfg* PiRailFactory::getNewOutCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new OutCfg") );
  return new OutCfg();
}

OutPin* PiRailFactory::getNewOutPin( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new OutPin") );
  return new OutPin();
}

Param* PiRailFactory::getNewParam( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Param") );
  return new Param();
}

Point* PiRailFactory::getNewPoint( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Point") );
  return new Point();
}

Port* PiRailFactory::getNewPort( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new Port") );
  return new Port();
}

PortCfg* PiRailFactory::getNewPortCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new PortCfg") );
  return new PortCfg();
}

PortPin* PiRailFactory::getNewPortPin( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new PortPin") );
  return new PortPin();
}

RangeAction* PiRailFactory::getNewRangeAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new RangeAction") );
  return new RangeAction();
}

SecurityCfg* PiRailFactory::getNewSecurityCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new SecurityCfg") );
  return new SecurityCfg();
}

SensorAction* PiRailFactory::getNewSensorAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new SensorAction") );
  return new SensorAction();
}

SetCmd* PiRailFactory::getNewSetCmd( Model* forParent, const QName* forRelation ) {
  Cmd* cmd = (Cmd*) forParent;
  SetCmd* result = (SetCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new SetCmd") );
    result = new SetCmd();
  }
  else {
    cmd->removeChild( result );
  }
  return result;
}

SetIO* PiRailFactory::getNewSetIO( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new SetIO") );
  return new SetIO();
}

State* PiRailFactory::getNewState( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new State") );
  return new State();
}

StateScript* PiRailFactory::getNewStateScript( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new StateScript") );
  return new StateScript();
}

SyncCmd* PiRailFactory::getNewSyncCmd( Model* forParent, const QName* forRelation ) {
  Cmd* cmd = (Cmd*) forParent;
  SyncCmd* result = (SyncCmd*) findFree( forRelation );
  if (result == NULL) {
    if (debug_heap) printHeap( F("Creating new SyncCmd") );
    result = new SyncCmd();
  }
  else {
    cmd->removeChild( result );
  }
  return result;
}

TimerAction* PiRailFactory::getNewTimerAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new TimerAction") );
  return new TimerAction();
}

TrackMsg* PiRailFactory::getNewTrackMsg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new TrackMsg") );
  return new TrackMsg();
}

TrackPos* PiRailFactory::getNewTrackPos( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new TrackPos") );
  return new TrackPos();
}

TriggerAction* PiRailFactory::getNewTriggerAction( Model *forParent, const QName *forRelation ) {
  if (debug_heap) printHeap( F("Creating new TriggerAction") );
  return new TriggerAction();
}

WifiCfg* PiRailFactory::getNewWifiCfg( Model* forParent, const QName* forRelation ) {
  if (debug_heap) printHeap( F("Creating new WifiCfg") );
  return new WifiCfg();
}

//-------------------------------------------------------------------------------------

void PiRailFactory::addFree( Model* model ) {
  model->interalSetNextModel( this->firstFree );
  this->firstFree = model;
}

Model* PiRailFactory::findFree( const QName* relationName ) {
  Model* model = this->firstFree;
  Model* prev = NULL;
  while (model != NULL) {
    if (model->parentRelation() == relationName) {
      if (prev == NULL) {
        this->firstFree = model->nextSibling( NULL );
      }
      else {
        prev->interalSetNextModel( model->nextSibling(NULL ));
      }
      return model;
    }
    prev = model;
    model = model->nextSibling( NULL );
  }
  return NULL;
}

void PiRailFactory::clearCmd( Cmd* cmd ) {
  Model* model = cmd->firstChild( NULL );
  while (model != NULL) {
    Model* next = model->nextSibling( NULL );
    cmd->removeChild( model );
    addFree( model );
    model = next;
  }
}

void PiRailFactory::clearState( State* state ) {
  Model* model = state->firstChild( NULL );
  while (model != NULL) {
    Model* next = model->nextSibling( NULL );
    state->removeChild( model );
    addFree( model );
    model = next;
  }
}
