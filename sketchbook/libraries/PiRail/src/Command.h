// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#ifndef COMMAND_H
#define COMMAND_H

#include <string.h>
#include "../../PiMobile/src/Model.h"
#include "../../PiMobile/src/XmlReader.h"
#include "Model.h"
#include "PiRailFactory.h"
#include "XmlReader.h"

class Command : public Model {

protected:

public:
  Command();

  virtual const QName* typeID();
  virtual void parseAttr( XmlReader* xmlReader );
  virtual void writeAttr( XmlWriter* xmlWriter );
//----------------------------------------------------------
// Manual extensions
public:
  virtual void execute( ItemConn* itemConn, Cfg* cfg, IoCfg* ioCfg, const QName* locker ) = 0;
  Action* getOwningAction();
};

#endif //COMMAND_H
