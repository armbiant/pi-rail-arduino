//
// Created by pru on 08.01.21.
//

#ifndef SKETCHBOOK_PIRAILSKETCH_H
#define SKETCHBOOK_PIRAILSKETCH_H

#include "ExtCfg.h"
#include "InCfg.h"
#include "MotorAction.h"
#include "OutCfg.h"
#include "PortCfg.h"
#include "SensorAction.h"
#include "TimerAction.h"

//-------- Declaration of callback functions --------
extern void initOutput( OutCfg* pinCfg );
extern void initInput( InCfg* pinCfg );
extern bool attachOutPin( OutCfg* outCfg, outMode_t outMode );
extern bool attachInPin( InCfg* inCfg, inModeType_t inMode, fireType_t fireType, bool useInterrupt );
extern bool attachPort( PortCfg* portCfg, portMode_t portMode );
extern int readMotorSensor( PortCfg* portCfg, int currentSpeed );
extern const QName* checkIDReader( SensorAction* sensorAction );
extern void processMsgState( MsgState* msgState );
extern void doMotor( PortCfg* portCfg, char dir, int motorOutput );
extern void doOutputPin( OutCfg* outCfg, int pinValue, int duration );
extern void sendUdpMessage( const char *msg );
extern void doSendMsg( PortCfg* portCfg, const char* msg );
extern void debugMsg( const char* msg, const QName* id, int val );

extern int getHeapSize();

extern void fatalError( const __FlashStringHelper* msg );
extern void printHeap( const __FlashStringHelper* msg );

#endif //SKETCHBOOK_PIRAILSKETCH_H
