// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

#ifndef FILEDEF_H
#define FILEDEF_H

#include <string.h>
#include "../../PiMobile/src/Model.h"
#include "../../PiMobile/src/XmlReader.h"
#include "Model.h"
#include "PiRailFactory.h"
#include "XmlReader.h"

class FileDef : public Model {

protected:
  char name[NAME_MAX_LEN+1];
  bool deletable = false;

public:
  FileDef();

  virtual const QName* typeID();
  virtual void parseAttr( XmlReader* xmlReader );
  virtual void writeAttr( XmlWriter* xmlWriter );
  const char* getName();
  void setName( char* name );
  bool getDeletable();
  void setDeletable( bool deletable );
//----------------------------------------------------------
// Manual extensions
};

#endif //FILE_H
