//
// Created by pru on 21.04.19.
//

#ifndef PIRAIL_QNAME_H
#define PIRAIL_QNAME_H

#include <stddef.h>
#include "StringHash.h"

class Namespace;

class QName {

private:
  Namespace*  _namespace;
  const char* _name;
  int         _hashCode = 0;
  QName*       _next = NULL; friend StringHash;
public:
  QName( Namespace *namespacePtr, const char *name ); friend StringHash;

public:
  Namespace* getNamespace() const;
  const char* getName() const;
  int hashCode() const;

  bool equals( const char *name ) const;

  bool equals( char *buffer, int startIndex, int endIndex ) const;
};


#endif //PIRAIL_QNAME_H
