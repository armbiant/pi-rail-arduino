//
// Created by pru on 13.07.19.
//

#include "Model.h"

Model* Model::createChild( const QName* tag ) {
  return NULL;
}

void Model::skipTag( XmlReader* xmlReader, const QName * skipTag ) {
  if (xmlReader->hasChildren()) {
    const QName *tag = xmlReader->nextTag( skipTag );
    while (tag != NULL) {
      this->skipTag( xmlReader, tag );
      tag = xmlReader->nextTag( skipTag );
    }
  }
}

void Model::parse( XmlReader* xmlReader, const QName * myTag ) {
  parseAttr( xmlReader );
  if (xmlReader->hasChildren()) {
    Model* lastCh = NULL;
    const QName *tag = xmlReader->nextTag( myTag );
    while (tag != NULL) {
      Model* newChild = createChild( tag );
      if (newChild == NULL) {
        Serial.print("Skiping tag=");
        Serial.println( tag->getName() );
        this->skipTag( xmlReader, tag );
      }
      else {
        newChild->parse( xmlReader, tag );
        this->addChild( tag, newChild, lastCh );
        lastCh = newChild;
      }
      tag = xmlReader->nextTag( myTag );
    }
  }
}

void Model::writeXml( const QName* relationName, XmlWriter* xmlWriter ) {
  xmlWriter->startTag( relationName );
  writeAttr( xmlWriter );
  Model* child = this->firstChild( NULL );
  if (child == NULL) {
    xmlWriter->endAttributes( true );
  }
  else {
    xmlWriter->endAttributes( false );
    while (child != NULL) {
      child->writeXml( child->parentRelation(), xmlWriter );
      child = child->nextSibling( NULL );
    }
    xmlWriter->endTag( relationName );
  }
}

Model* Model::parent() {
  return this->parentModel;
}

const QName* Model::parentRelation(){
  return this->parentRelationName;
}

void Model::interalSetNextModel( Model* next ) {
  this->nextModel = next;
}

Model* Model::nextSibling( const QName* relationName ) {
  Model* next = this->nextModel;
  while (next != NULL) {
    if ((relationName == NULL) || (next->parentRelationName == relationName)) {
      return next;
    }
    next = next->nextModel;
  }
  return NULL;
}

int Model::childCount( const QName* relationName ) {
  int count = 0;
  Model* child = this->firstCh;
  while (child != NULL) {
    if ((relationName == NULL) || (child->parentRelationName == relationName)) {
      count++;
    }
    child = child->nextModel;
  }
  return count;
}

Model* Model::childAt( const QName* relationName, int index ) {
  int pos = 0;
  Model* child = this->firstCh;
  while (child != NULL) {
    if ((relationName == NULL) || (child->parentRelationName == relationName)) {
      if (pos == index) {
        return child;
      }
      else {
        pos++;
      }
    }
    child = child->nextModel;
  }
  return NULL;
}

Model* Model::firstChild( const QName* relationName ) {
  return childAt( relationName, 0 );
}

Model* Model::lastChild( const QName* relationName ) {
  Model* child = this->firstCh;
  Model* lastChild = child;
  while (child != NULL) {
    if ((relationName == NULL) || (child->parentRelationName == relationName)) {
      lastChild = child;
    }
    child = child->nextModel;
  }
  return lastChild;
}

void Model::addChild( const QName* relationName, Model* newChild, Model* afterChild ) {
  newChild->parentModel = this;
  newChild->parentRelationName = (QName*) relationName;
  if (afterChild == NULL) {
    newChild->nextModel = this->firstCh;
    this->firstCh = newChild;
  }
  else if (this->firstCh == NULL) {
    this->firstCh = newChild;
    newChild->nextModel = NULL;
  }
  else if (afterChild->parentModel == this) {
    newChild->nextModel = afterChild->nextModel;
    afterChild->nextModel = newChild;
  }
  else {
    afterChild = this->lastChild( NULL );
    newChild->nextModel = afterChild->nextModel;
    afterChild->nextModel = newChild;
  }
}

void Model::removeChild( Model* oldChild ) {
  Model* child = this->firstCh;
  if (child == oldChild) {
    this->firstCh = oldChild->nextModel;
  }
  Model* prevChild = child;
  while (child != NULL) {
    child = prevChild->nextModel;
    if (child == oldChild) {
      prevChild->nextModel = oldChild->nextModel;
      child = NULL;
    }
    prevChild = child;
  }
  oldChild->nextModel = NULL;
  oldChild->parentModel = NULL;
  // do not clear parentRelationName - needed for reuse
}

