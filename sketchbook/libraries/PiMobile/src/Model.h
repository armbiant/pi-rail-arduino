//
// Created by pru on 13.07.19.
//

#ifndef SKETCHBOOK_MODEL_H
#define SKETCHBOOK_MODEL_H


#include "XmlReader.h"
#include "XmlWriter.h"

class Model {
private:
  QName* parentRelationName = NULL;
  Model* parentModel = NULL;
  Model* nextModel = NULL;
  Model* firstCh = NULL;
public:
  virtual const QName* typeID() = 0;
  virtual void parseAttr( XmlReader* xmlReader ) = 0;
  virtual Model* createChild( const QName* tag );
  virtual void writeAttr( XmlWriter* xmlReader ) = 0;

  void parse( XmlReader* xmlReader, const QName * myTag );
  void skipTag( XmlReader* xmlReader, const QName * skipTag );
  void writeXml( const QName* relationName, XmlWriter* xmlWriter );

  Model* parent();
  void interalSetNextModel( Model* next );
  const QName* parentRelation();
  Model* nextSibling( const QName* relationName );
  Model* childAt( const QName* relationName, int index );
  Model* firstChild( const QName* relationName );
  Model* lastChild( const QName* relationName );
  int childCount( const QName* relationName );
  void addChild( const QName* relationName, Model* newChild, Model* afterChild );
  void removeChild( Model* child );
};


#endif //SKETCHBOOK_MODEL_H
