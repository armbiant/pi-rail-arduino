/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <string.h>
#include "Namespace.h"

Namespace::Namespace( const char *uri ) {
  _uri = uri;
  _stringHash = new StringHash( this );
}

const QName *Namespace::getQName( char *txtBuf, int startPos, int endPos ) {
  return _stringHash->put( txtBuf, startPos, endPos );
}

const QName *Namespace::getQName( const char *name ) {
  return _stringHash->put( name );
}

const char *Namespace::getUri() {
  return _uri;
}

//----- Static code --------------------------------------

int Namespace::nsCount = 0;
Namespace* Namespace::nsTable[NS_MAX_COUNT];

Namespace* Namespace::getInstance( const char* nsUri ) {
  for (int i = 0; i < nsCount; i++) {
    if (strcmp( nsTable[i]->getUri(), nsUri ) == 0) {
      return nsTable [i];
    }
  }
  if (nsCount < NS_MAX_COUNT) {
    auto newNS = new Namespace( nsUri );
    nsTable[nsCount] = newNS;
    nsCount++;
    return newNS;
  }
  else {
    return NULL;
  }
}

Namespace* Namespace::getInstance( char *buf, int startPos, int endPos ) {
  int len = endPos - startPos;
  for (int i = 0; i < nsCount; i++) {
    if (strncmp( nsTable[i]->getUri(), &buf[startPos], len ) == 0) {
      return nsTable [i];
    }
  }
  if (nsCount < NS_MAX_COUNT) {
    char* nsUri = new char[len+1];
    strncpy( nsUri, &buf[startPos], len );
    nsUri[len] = 0;
    auto newNS = new Namespace( nsUri );
    nsTable[nsCount] = newNS;
    nsCount++;
    return newNS;
  }
  else {
    return NULL;
  }
}

