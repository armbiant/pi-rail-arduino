/*
 * This file is part of PI-Rail-Arduino (https://gitlab.com/pi-rail/pi-rail-arduino).
 * Copyright (c) 2013-2020 PI-Data AG, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PIRAIL_STRINGHASH_H
#define PIRAIL_STRINGHASH_H

const int TABLE_SIZE = 20;

class Namespace;
class QName;

/**
 * Calculates the hash code for the characters of value
 *
 * @param value      the value from which the has code is calculated
 * @return the calculated hash code
 */
int calcHashCode( const char value[] );

class StringHash {
private:
  /**
   * The uri this hash binding is used for
   */
  Namespace* _namespace;

  /**
   * The total number of entries in the hash binding.
   */
  int count;

  /**
   * The binding is rehashed when its size exceeds this threshold.  (The
   * value of this field is (int)(capacity * loadFactor).)
   */
  int threshold;

  /**
   * The load factor for the hashtable in per cent.
   */
  int loadFactor;

  /**
   * The number of times this Hashtable has been structurally modified
   * Structural modifications are those that change the number of entries in
   * the Hashtable or otherwise modify its internal structure (e.g.,
   * rehash).  This field is used to make iterators on Collection-views of
   * the Hashtable fail-fast.  (See ConcurrentModificationException).
   */
  int modCount = 0;

  int _capacity = TABLE_SIZE;
  /**
   * The hash binding data.
   */
  QName* table[TABLE_SIZE];

public:
  /**
   * Constructs a new, empty hashtable with the specified initial
   * capacity and the specified load factor.
   *
   * @param ns the uri this hash binding is used for
   */
  StringHash( Namespace* ns );

  /**
   * Returns the number of keys in this hashtable.
   *
   * @return  the number of keys in this hashtable.
   */
  int size();

  /**
   * Returns the QName for value in this binding or null
   *
   * @param   value   the value to look for
   * @return  the QName for value in this binding or null
   */
  const QName *get( const char *value );

  /**
   * Returns the value to which the specified key is mapped in this hashtable.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return  the value to which the key is mapped in this hashtable;
   *          <code>null</code> if the key is not mapped to any value in
   *          this hashtable.
   */
  const QName* get( char buffer[], int startIndex, int endIndex );

  /**
   * Looks for a QName having a name equal to the character sequence
   * between startIndex (inclusive) and endIndex (exclusive) inside buffer.
   * If that QName does not exist it is created.
   *
   * If a new StrignID shall not be created use teh <code>get</code> method.
   *
   * @param buffer      the buffer containing the character sequence
   * @param startIndex index of the first character inside buffer
   * @param endIndex   1 + index of the last character inside buffer
   * @return the QName for the character sequence
   * @exception  NullPointerException  if the buffer is null
   * @see     Object#equals(Object)
   */
  const QName* put( char buffer[], int startIndex, int endIndex );

  /**
   * Looks for a QName having a name equal value.
   * If that QName does not exist it is created.
   *
   * If a new StrignID shall not be created use the <code>get</code> method.
   *
   * @param value   the value to be put into the binding
   * @return the QName for the character sequence
   * @exception  NullPointerException  if the value is null.
   * @see     Object#equals(Object)
   */
  const QName* put( const char* value );
};


#endif //PIRAIL_STRINGHASH_H
